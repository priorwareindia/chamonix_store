﻿function getCardType(accountNumber) {
    var result = "Card details not correct";
    var one_digit = accountNumber.substring(0, 1);
    var two_digit = accountNumber.substring(0, 2);
    var three_digit = accountNumber.substring(0, 3);
    var four_digit = accountNumber.substring(0, 4);
    var flag = 0;

    if (flag == 0) {
        switch (two_digit) {
            case '34':
            case '37':
                {

                    if (accountNumber.length == 15) { result = "AE"; flag = 1; }
                    else { result = "Select"; }

                }
                break;

            case '51':
            case '52':
            case '53':
            case '54':
            case '55': result = "MC";
                flag = 1;
                break;


            default: result = "Select";
                break;
        }
    }
    if (flag == 0) {
        switch (three_digit) {
            case '304': 
            case '305' : result = "DC";
                flag = 1;
                break;
             
            default: result = "Select";
                break;

        }
    }
    if (flag == 0) {
        switch (four_digit) {
            case '6011': result = "DI";
                flag = 1;
                break;
            case '5020':
            case '5038': 
            case '6304': 
            case '6759': result = "M";
                flag=1;
                break;
            default: result = "Select";
                break;

        }
    }
    if (flag == 0) {
        switch (one_digit) {
            case '4': result = "VI";
                flag = 1;
                break;
            default: result = "Select";
                break;

        }
    }
    flag = 0;
   
    return result;
    
}
function selectCardType(CardType, ddlCardType) {
    switch (CardType) {
  
        case 'Select':
            {
                alert("Fill Correct Card Number");
                ddlCardType.selectedIndex = 0;
            }
            break;
        case 'AE': ddlCardType.selectedIndex = 1;
            break;
        case 'Discover': ddlCardType.selectedIndex = 2;
            break;
        case 'MasterCard': ddlCardType.selectedIndex = 4;
            break;
        case 'Visa': ddlCardType.selectedIndex = 5;
            break;
        default:
            {
                alert("Fill Correct Card Number");
                ddlCardType.selectedIndex = 0;
            }
            break;
    }
}