﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:HiddenField ID="HiddenField1PhoneOrder" runat="server" />
            <asp:FileUpload ID="FileUpload1PhoneOrder" runat="server" CssClass="btn btn-default  btn-sm" />
            <asp:Button ID="btnUplaodPhoneOrder" runat="server" Text="Upload Order File" CssClass="btn btn-primary btn-sm" OnClick="btnUplaodPhoneOrder_Click" />
            <asp:Button ID="btnprocess" runat="server" Text="Process" CssClass="btn btn-primary btn-sm" OnClick="btnprocess_Click" />
            <br />
            <br />
            <hr />
                <asp:DataPagerGridView ID="gridcarddetail" runat="server" Width="100%" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="CustID" HeaderText="Customer Id" />
                    <asp:BoundField DataField="name" HeaderText="Name" />
                    <asp:BoundField DataField="PhoneNo" HeaderText="Phone No" />
                    <asp:BoundField DataField="PhoneNo1" HeaderText="Mobile No" />
                    <asp:BoundField DataField="OfficeNoB" HeaderText="Office No" />
                    <asp:BoundField DataField="MomOrderNo" HeaderText="Mom Order No" />
                    <asp:BoundField DataField="CardType" HeaderText="Card Type" />
                    <asp:BoundField DataField="exp" HeaderText="Card Expiry" />
                </Columns>
            </asp:DataPagerGridView>
        </div>
       
    </form>
</body>
</html>
