﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExceptionPage.aspx.cs" Inherits="ExceptionPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <center>
       <h1>
                    <asp:Label ID="lblException" runat="server" Text="Something went wrong... :(" ForeColor="Red"
                        Font-Size="12pt"></asp:Label>
             </h1>   
               <div align="center" style="margin-top: 10%">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/ScriptsAndCSS/CSS/Images/404-error-redirect-to-homepage.jpg"  />
                    <a id="A1" runat="server" href="~/AM/SignIn.aspx">Sign In</a>
              </div>
        </center>
    </div>
    </form>
</body>
</html>
