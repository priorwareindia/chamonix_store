﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyLogin
/// </summary>
public class PropertyLogin:PropertyUtilityAbstract
{
    string  emailID, zipcode;

   
    public string EmailID
    {
        get { return emailID; }
        set { emailID = value; }
    }
    public string IpAddress { get; set; }
    public string Latitude { get; set; }

    public string Longitude { get; set; }
    public string Zipcode
    {
        get { return zipcode; }
        set { zipcode = value; }
    }



}