﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertInvoiceMaster
/// </summary>
public class PropertyInvoiceMaster : PropertyUtilityAbstract
{
    DateTime cancelDate, toDate, fromDate, toholddate, fromholddate;
    bool backOrder;
    Int64 orderitemNo;
    int pd_ItemNo, itemno, shippingInterval;
    string invoiceNo, orderStatus, sYSorderno, firstName, billingEntryID, shippingEntryID, creditEntryID,
        approveCode, subtotalamt, shipamt, taxamt, billTotalamt, custID, shipVIA, trackingNo, orderType,
        discountAmt, invoiceDate, paymentID, paymentMethod, paymentType, invoiceStatus, cancelInvoiceNo,
        cancelDesc, productid, shippingDate, stateTax, accountNo, routingNo, checkNo, shippingType, shippingQty,
        shippingcost, invnote, invoiceType, refrenceNo, transactionID, quantity, skuID, salesID, terms, referenceNo,
        barcodeNo, holdDesc, holdTillDate;

    decimal discountPer, price, spUprice, actualCharged, credit, amount, chargedAmt, extrashipping, overalldiscount, noOfHappyPts;


    public string
        SourceKey
    {
        get;
        set;
    }
    public string CustInvoiceType { get; set; }
    public string AutoshipType { get; set; }
    public string HearAboutUs { get; set; }
    public string AmountType { get; set; }

    public string SKU { get; set; }
    public string
        SaleSource
    {
        get;
        set;
    }
    public int ItemNo
    {
        get { return itemno; }
        set { itemno = value; }
    }
    public string OrderMemo
    {
        get;
        set;
    }
    public string SKUXml
    {
        get;
        set;
    }
    public string SKuProductXml
    {
        get;
        set;
    }
    public string TransactionType
    {
        get;
        set;
    }
    public int Pd_ItemNo
    {
        get { return pd_ItemNo; }
        set { pd_ItemNo = value; }
    }
    public Int64 OrderitemNo
    {
        get { return orderitemNo; }
        set { orderitemNo = value; }
    }

    public string PaymentType
    {
        get { return paymentType; }
        set { paymentType = value; }
    }
    public decimal Amount
    {
        get { return amount; }
        set { amount = value; }
    }

    public decimal Credit
    {
        get { return credit; }
        set { credit = value; }
    }
    int contistatus = 1;
    bool usePrice;

    public decimal ActualCharged
    {
        get { return actualCharged; }
        set { actualCharged = value; }
    }

    public bool BackOrder
    {
        get { return backOrder; }
        set { backOrder = value; }
    }
    public string ApproveCode
    {
        get { return approveCode; }
        set { approveCode = value; }
    }

    public string BarcodeNo
    {
        get { return barcodeNo; }
        set { barcodeNo = value; }
    }

    public string CheckNo
    {
        get { return checkNo; }
        set { checkNo = value; }
    }

    public string RoutingNo
    {
        get { return routingNo; }
        set { routingNo = value; }
    }

    public string AccountNo
    {
        get { return accountNo; }
        set { accountNo = value; }
    }
    public string PaymentMethod
    {
        get { return paymentMethod; }
        set { paymentMethod = value; }
    }

    public string PaymentID
    {
        get { return paymentID; }
        set { paymentID = value; }
    }

    public decimal NoOfHappyPts
    {
        get { return noOfHappyPts; }
        set { noOfHappyPts = value; }
    }

    public decimal Price
    {
        get { return price; }
        set { price = value; }
    }
    public decimal SPUnitPrice
    {
        get { return spUprice; }
        set { spUprice = value; }
    }


    public string ReferenceNo
    {
        get { return referenceNo; }
        set { referenceNo = value; }
    }

    public string CreditEntryID
    {
        get { return creditEntryID; }
        set { creditEntryID = value; }
    }

    public string BillingEntryID
    {
        get { return billingEntryID; }
        set { billingEntryID = value; }
    }

    public string ShippingEntryID
    {
        get { return shippingEntryID; }
        set { shippingEntryID = value; }
    }

    public bool UsePrice
    {
        get { return usePrice; }
        set { usePrice = value; }
    }

    public string OrderStatus
    {
        get { return orderStatus; }
        set { orderStatus = value; }
    }
    public DateTime ToDate
    {
        get { return toDate; }
        set { toDate = value; }
    }
    public DateTime FromDate
    {
        get { return fromDate; }
        set { fromDate = value; }
    }
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }
    public string InvoiceDate
    {
        get { return invoiceDate; }
        set { invoiceDate = value; }
    }
    public string ProductId
    {
        get { return productid; }
        set { productid = value; }
    }
    public string Terms
    {
        get { return terms; }
        set { terms = value; }
    }
    public int CountiStatus
    {
        get { return contistatus; }
        set { contistatus = value; }
    }
    public string SalesID
    {
        get { return salesID; }
        set { salesID = value; }
    }
    public decimal DiscountPer
    {
        get { return discountPer; }
        set { discountPer = value; }
    }
    public string DiscountAmt
    {
        get { return discountAmt; }
        set { discountAmt = value; }
    }
    public string Quantity
    {
        get { return quantity; }
        set { quantity = value; }
    }
    public string SkuID
    {
        get { return skuID; }
        set { skuID = value; }
    }
    public string InvoiceNo
    {
        get { return invoiceNo; }
        set { invoiceNo = value; }
    }
    public string SYSorderno
    {
        get { return sYSorderno; }
        set { sYSorderno = value; }
    }
    public string Subtotalamt
    {
        get { return subtotalamt; }
        set { subtotalamt = value; }
    }
    public string Shipamt
    {
        get { return shipamt; }
        set { shipamt = value; }
    }
    public string Taxamt
    {
        get { return taxamt; }
        set { taxamt = value; }
    }
    public decimal TaxableAmount
    { get; set; }
    public string BillTotalamt
    {
        get { return billTotalamt; }
        set { billTotalamt = value; }
    }
    public string CustID
    {
        get { return custID; }
        set { custID = value; }
    }
    public string ShipVIA
    {
        get { return shipVIA; }
        set { shipVIA = value; }
    }
    public string ShippingDate
    {
        get { return shippingDate; }
        set { shippingDate = value; }
    }
    public string TrackingNo
    {
        get { return trackingNo; }
        set { trackingNo = value; }
    }
    public string OrderType
    {
        get { return orderType; }
        set { orderType = value; }
    }
    public string InvoiceStatus
    {
        get { return invoiceStatus; }
        set { invoiceStatus = value; }
    }
    public string CancelInvoiceNo
    {
        get { return cancelInvoiceNo; }
        set { cancelInvoiceNo = value; }
    }
    public string CancelDesc
    {
        get { return cancelDesc; }
        set { cancelDesc = value; }
    }
    public string StateTax
    {
        get { return stateTax; }
        set { stateTax = value; }
    }

    public string ShippingType
    {
        get { return shippingType; }
        set { shippingType = value; }
    }
    public int ShippingInterval
    {
        get { return shippingInterval; }
        set { shippingInterval = value; }
    }
    public string ShippingQty
    {
        get { return shippingQty; }
        set { shippingQty = value; }
    }
    public string Shippnigcost
    {
        get { return shippingcost; }
        set { shippingcost = value; }
    }
    public string Invnote
    {
        get { return invnote; }
        set { invnote = value; }
    }
    public string InvoiceType
    {
        get { return invoiceType; }
        set { invoiceType = value; }
    }
    public string RefrenceNo
    {
        get { return refrenceNo; }
        set { refrenceNo = value; }
    }
    public string TransactionID
    {
        get { return transactionID; }
        set { transactionID = value; }
    }
    public string RefundAgainstTransactionID
    { get; set; }

    public DateTime CancelDate
    {
        get { if (cancelDate == null || cancelDate == DateTime.MinValue) { cancelDate = Convert.ToDateTime("01/01/2010"); } return cancelDate; }
        set { if (cancelDate == null || cancelDate == DateTime.MinValue) { cancelDate = Convert.ToDateTime("01/01/2010"); } cancelDate = value; }
    }


    //----------HOLD ORDER/INVOICE
    public string HoldDesc
    {
        get { return holdDesc; }
        set { holdDesc = value; }
    }

    public string HoldTillDate
    {
        get { return holdTillDate; }
        set { holdTillDate = value; }
    }
    public DateTime ToHoldDate
    {
        get { return toholddate; }
        set { toholddate = value; }
    }
    public DateTime FromHoldDate
    {
        get { return fromholddate; }
        set { fromholddate = value; }
    }



    public decimal ChargedAmount
    {
        get { return chargedAmt; }
        set { chargedAmt = value; }
    }

    public int InvoiceProcessconti
    {
        get;
        set;
    }
    public int TimeInterval
    {
        get;
        set;
    }
    public string SKUType
    {
        get;
        set;
    }
    public string TempHoldDate
    {
        get;
        set;
    }
    public string FromPage
    {
        get;
        set;
    }
    public decimal Extrashipping
    {
        get { return extrashipping; }
        set { extrashipping = value; }
    }
    public decimal OverallDiscount
    {
        get { return overalldiscount; }
        set { overalldiscount = value; }
    }
    public int OneTiemOnly
    {
        get;
        set;
    }
}