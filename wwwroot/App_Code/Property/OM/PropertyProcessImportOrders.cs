﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyProcessImportOrders
/// </summary>
public class PropertyProcessImportOrders : PropertyUtilityAbstract
{
    DateTime orderDate, orderProcessingDate, cancelDate, creationDate, orderHoldDate, toDate, fromDate, toholddate, fromholddate;
    int customerType;
    string custID, sYSOrderNo, cancelDesc, digit4, custNum, altNum, lastName, firstName, dateOfBirth, address1, address2, city, state, zipCode, orderID, phone1, phone2, emailID, comment, greetings, paymentMethod, cardType, cardNum, expires, sourceKey, salesID, shipVIA, paid, createdby, sku,
        product01, quantity01, product02, quantity02, product03, quantity03, product04, quantity04, product05, quantity05, product06, quantity06, product07, quantity07, product08, quantity08, product09, quantity09, product10, quantity10, continued, transactionID, orderStatus, orderType, sLastName, sFirstName, sAddress1, sAddress2, sCity, sState, sZip, cardHolder, routingNum, accountNum,
        accountType, bankName, checkNo, moneyOrdNo, mediaCode, dNIS, invoiceNo, country, sCountry, sPhone1, sPhone2, sEmailID, price01, price02, price03, price04, price05, discount01, discount02, discount03, discount04, discount05, price06, price07, price08, price09, price10, discount06, discount07, discount08, discount09, discount10, orderNote, shipping, credit, invGenerated, coupancode, approvecode, useshippingcheck;
    bool usePrice, useShipping;
    decimal happyPoints, subTotal, overalldiscount, coupanamount;

    public decimal SubTotal
    {
        get { return subTotal; }
        set { subTotal = value; }
    }
    public string Credit
    {
        get { return credit; }
        set { credit = value; }
    }

    public decimal HappyPoints
    {
        get { return happyPoints; }
        set { happyPoints = value; }
    }
    public DateTime ToHoldDate
    {
        get { return toholddate; }
        set { toholddate = value; }
    }
    public DateTime FromHoldDate
    {
        get { return fromholddate; }
        set { fromholddate = value; }
    }
    public DateTime ToDate
    {
        get { return toDate; }
        set { toDate = value; }
    }
    public DateTime FromDate
    {
        get { return fromDate; }
        set { fromDate = value; }
    }

    public string DNIS
    {
        get { return dNIS; }
        set { dNIS = value; }
    }
    public string MoneyOrdNo
    {
        get { return moneyOrdNo; }
        set { moneyOrdNo = value; }
    }
    public string HearAboutUs
    {
        get;
        set;
    }
    public string OrderOriginalSource
    {
        get;
        set;
    }
    public string CheckNo
    {
        get { return checkNo; }
        set { checkNo = value; }
    }
    public string OrderID
    {
        get { return orderID; }
        set { orderID = value; }
    }
    public string Continued
    {
        get { return continued; }
        set { continued = value; }
    }
    public string Shipping
    {
        get { return shipping; }
        set { shipping = value; }
    }
    public bool UseShipping
    {
        get { return useShipping; }
        set { useShipping = value; }
    }
    public string OrderNote
    {
        get { return orderNote; }
        set { orderNote = value; }
    }
    public bool UsePrice
    {
        get { return usePrice; }
        set { usePrice = value; }
    }

    public DateTime OrderHoldDate
    {
        get { if (orderHoldDate == null || orderHoldDate == DateTime.MinValue) { orderHoldDate = Convert.ToDateTime("01/01/2010"); } return orderHoldDate; }
        set { if (orderHoldDate == null || orderHoldDate == DateTime.MinValue) { orderHoldDate = Convert.ToDateTime("01/01/2010"); } orderHoldDate = value; }
    }
    public string Price01
    {
        get { return price01; }
        set { price01 = value; }
    }
    public string Price02
    {
        get { return price02; }
        set { price02 = value; }
    }
    public string Price03
    {
        get { return price03; }
        set { price03 = value; }
    }
    public string Price04
    {
        get { return price04; }
        set { price04 = value; }
    }
    public string Price05
    {
        get { return price05; }
        set { price05 = value; }
    }
    public string Discount01
    {
        get { return discount01; }
        set { discount01 = value; }
    }
    public string Discount02
    {
        get { return discount02; }
        set { discount02 = value; }
    }
    public string Discount03
    {
        get { return discount03; }
        set { discount03 = value; }
    }
    public string Discount04
    {
        get { return discount04; }
        set { discount04 = value; }
    }
    public string Discount05
    {
        get { return discount05; }
        set { discount05 = value; }
    }

    public string Price06
    {
        get { return price06; }
        set { price06 = value; }
    }
    public string Price07
    {
        get { return price07; }
        set { price07 = value; }
    }
    public string Price08
    {
        get { return price08; }
        set { price08 = value; }
    }
    public string Price09
    {
        get { return price09; }
        set { price09 = value; }
    }
    public string Price10
    {
        get { return price10; }
        set { price10 = value; }
    }
    public string Discount06
    {
        get { return discount06; }
        set { discount06 = value; }
    }
    public string Discount07
    {
        get { return discount07; }
        set { discount07 = value; }
    }
    public string Discount08
    {
        get { return discount08; }
        set { discount08 = value; }
    }
    public string Discount09
    {
        get { return discount09; }
        set { discount09 = value; }
    }
    public string Discount10
    {
        get { return discount10; }
        set { discount10 = value; }
    }

    public string SEmailID
    {
        get { return sEmailID; }
        set { sEmailID = value; }
    }
    public string EmailID
    {
        get { return emailID; }
        set { emailID = value; }
    }
    public string SPhone2
    {
        get { return sPhone2; }
        set { sPhone2 = value; }
    }
    public string PaymentMethod
    {
        get { return paymentMethod; }
        set { paymentMethod = value; }
    }
    public string Greetings
    {
        get { return greetings; }
        set { greetings = value; }
    }
    public string Phone1
    {
        get { return phone1; }
        set { phone1 = value; }
    }
    public string Phone2
    {
        get { return phone2; }
        set { phone2 = value; }
    }
    public string DateOfBirth
    {
        get { return dateOfBirth; }
        set { dateOfBirth = value; }
    }
    public DateTime CreationDate
    {
        get { if (creationDate == null || creationDate == DateTime.MinValue) { creationDate = Convert.ToDateTime("01/01/2010"); } return creationDate; }
        set { if (creationDate == null || creationDate == DateTime.MinValue) { creationDate = Convert.ToDateTime("01/01/2010"); } creationDate = value; }
    }
    public string Country
    {
        get { return country; }
        set { country = value; }
    }
    public string SCountry
    {
        get { return sCountry; }
        set { sCountry = value; }
    }
    public string SPhone1
    {
        get { return sPhone1; }
        set { sPhone1 = value; }
    }
    public string InvoiceNo
    {
        get { return invoiceNo; }
        set { invoiceNo = value; }
    }
    public string CustID
    {
        get { return custID; }
        set { custID = value; }
    }
    public string SYSOrderNo
    {
        get { return sYSOrderNo; }
        set { sYSOrderNo = value; }
    }
    public DateTime OrderProcessingDate
    {
        get { if (orderProcessingDate == null || orderProcessingDate == DateTime.MinValue) { orderProcessingDate = Convert.ToDateTime("01/01/2010"); } return orderProcessingDate; }
        set { if (orderProcessingDate == null || orderProcessingDate == DateTime.MinValue) { orderProcessingDate = Convert.ToDateTime("01/01/2010"); } orderProcessingDate = value; }
    }
    public DateTime CancelDate
    {
        get { if (cancelDate == null || cancelDate == DateTime.MinValue) { cancelDate = Convert.ToDateTime("01/01/2010"); } return cancelDate; }
        set { if (cancelDate == null || cancelDate == DateTime.MinValue) { cancelDate = Convert.ToDateTime("01/01/2010"); } cancelDate = value; }
    }
    public string TransactionID
    {
        get { return transactionID; }
        set { transactionID = value; }
    }
    public string OrderStatus
    {
        get { return orderStatus; }
        set { orderStatus = value; }
    }
    public string OrderType
    {
        get { return orderType; }
        set { orderType = value; }
    }
    public string CancelDesc
    {
        get { return cancelDesc; }
        set { cancelDesc = value; }
    }
    public string Digit4
    {
        get { return digit4; }
        set { digit4 = value; }
    }
    public string CustNum
    {
        get { return custNum; }
        set { custNum = value; }
    }
    public string AltNum
    {
        get { return altNum; }
        set { altNum = value; }
    }
    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }
    public string Address1
    {
        get { return address1; }
        set { address1 = value; }
    }
    public string Address2
    {
        get { return address2; }
        set { address2 = value; }
    }
    public string City
    {
        get { return city; }
        set { city = value; }
    }
    public string State
    {
        get { return state; }
        set { state = value; }
    }
    public string ZipCode
    {
        get { return zipCode; }
        set { zipCode = value; }
    }

    public string Comment
    {
        get { return comment; }
        set { comment = value; }
    }
    public string CardType
    {
        get { return cardType; }
        set { cardType = value; }
    }
    public string CardNum
    {
        get { return cardNum; }
        set { cardNum = value; }
    }
    public string Expires
    {
        get { return expires; }
        set { expires = value; }
    }
    public string SourceKey
    {
        get { return sourceKey; }
        set { sourceKey = value; }
    }
    public string SalesID
    {
        get { return salesID; }
        set { salesID = value; }
    }
    public string ShipVIA
    {
        get { return shipVIA; }
        set { shipVIA = value; }
    }
    public string Paid
    {
        get { return paid; }
        set { paid = value; }
    }
    public string Product01
    {
        get { return product01; }
        set { product01 = value; }
    }
    public string Quantity01
    {
        get { return quantity01; }
        set { quantity01 = value; }
    }
    public string Product02
    {
        get { return product02; }
        set { product02 = value; }
    }
    public string Quantity02
    {
        get { return quantity02; }
        set { quantity02 = value; }
    }
    public string Product03
    {
        get { return product03; }
        set { product03 = value; }
    }
    public string Quantity03
    {
        get { return quantity03; }
        set { quantity03 = value; }
    }
    public string Product04
    {
        get { return product04; }
        set { product04 = value; }
    }
    public string Quantity04
    {
        get { return quantity04; }
        set { quantity04 = value; }
    }
    public string Product05
    {
        get { return product05; }
        set { product05 = value; }
    }
    public string Quantity05
    {
        get { return quantity05; }
        set { quantity05 = value; }
    }

    public string Product06
    {
        get { return product06; }
        set { product06 = value; }
    }
    public string Quantity06
    {
        get { return quantity06; }
        set { quantity06 = value; }
    }
    public string Product07
    {
        get { return product07; }
        set { product07 = value; }
    }
    public string Quantity07
    {
        get { return quantity07; }
        set { quantity07 = value; }
    }
    public string Product08
    {
        get { return product08; }
        set { product08 = value; }
    }
    public string Quantity08
    {
        get { return quantity08; }
        set { quantity08 = value; }
    }
    public string Product09
    {
        get { return product09; }
        set { product09 = value; }
    }
    public string Quantity09
    {
        get { return quantity09; }
        set { quantity09 = value; }
    }
    public string Product10
    {
        get { return product10; }
        set { product10 = value; }
    }
    public string Quantity10
    {
        get { return quantity10; }
        set { quantity10 = value; }
    }

    public string SLastName
    {
        get { return sLastName; }
        set { sLastName = value; }
    }
    public string SFirstName
    {
        get { return sFirstName; }
        set { sFirstName = value; }
    }
    public string SAddress1
    {
        get { return sAddress1; }
        set { sAddress1 = value; }
    }
    public string SAddress2
    {
        get { return sAddress2; }
        set { sAddress2 = value; }
    }
    public string SCity
    {
        get { return sCity; }
        set { sCity = value; }
    }
    public string SState
    {
        get { return sState; }
        set { sState = value; }
    }
    public string SZip
    {
        get { return sZip; }
        set { sZip = value; }
    }
    public string CardHolder
    {
        get { return cardHolder; }
        set { cardHolder = value; }
    }
    public string RoutingNum
    {
        get { return routingNum; }
        set { routingNum = value; }
    }
    public string AccountNum
    {
        get { return accountNum; }
        set { accountNum = value; }
    }
    public string AccountType
    {
        get { return accountType; }
        set { accountType = value; }
    }
    public string BankName
    {
        get { return bankName; }
        set { bankName = value; }
    }
    public string MediaCode
    {
        get { return mediaCode; }
        set { mediaCode = value; }
    }
    public DateTime OrderDate
    {
        get { if (orderDate == null || orderDate == DateTime.MinValue) { orderDate = Convert.ToDateTime("01/01/2010"); } return orderDate; }
        set { if (orderDate == null || orderDate == DateTime.MinValue) { orderDate = Convert.ToDateTime("01/01/2010"); } orderDate = value; }
    }



    public string InvGenerated
    {
        get { return invGenerated; }
        set { invGenerated = value; }
    }
    public decimal Overalldiscount
    {
        get { return overalldiscount; }
        set { overalldiscount = value; }
    }
    public string Coupancode
    {
        get { return coupancode; }
        set { coupancode = value; }
    }
    public decimal Coupanamount
    {
        get { return coupanamount; }
        set { coupanamount = value; }
    }
    public string Approvecode
    {
        get { return approvecode; }
        set { approvecode = value; }
    }
    public string UseShippingcheck
    {
        get { return useshippingcheck; }
        set { useshippingcheck = value; }
    }
    public string Createdby
    {
        get { return createdby; }
        set { createdby = value; }
    }

    public string CustomerProfileId
    {
        get;
        set;
    }
    public string CustomerPaymentProfileId
    {
        get;
        set;
    }
    public int PaymentGateway
    { get; set; }
    public string CreditEntryID
    {
        get;
        set;
    }
    public string SkU
    {
        get;
        set;
    }
    public int CustomerType
    {
        get;
        set;
    }
    public string CategoryId
    {
        get;
        set;
    }
    public decimal Tax
    {
        get;
        set;
    }

    public int RowNumber { get; set; }
    public string SaleSource1 { get; set; }
    public int nooforder { get; set; }
    public double netsale { get; set; }
    public double TAX { get; set; }
    public double GROSSSsale { get; set; }
    public double TotalShiping { get; set; }
}