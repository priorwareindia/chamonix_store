﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyCustomerDetail
/// </summary>
public class PropertyCustomerDetail : PropertyUtilityAbstract
{
    DateTime todate, fromdate;
    bool doNotMail, doNotEmail, doNotCall, doNotSMS;
    string custID, firstName, lastName, phoneNo, phoneNo1, emailID, customerMedia, sourceKey, address, address1, city, state, bankEntryId, echeckId, officNoB, officeNoS, orderNo, transactionId,
        country, zip, cardType, nameOnCard, cardNo, cvvNo, sTFirstName, sTLastName, sTAddress, sTAddress1, sTCity, sTState, sTCountry, sTZip, sTphoneNo1, sTemailID, rAN, invoiceNo
        , sCompany, digit4, billEntryID, creditEntryID, shippingEntryID, cardExpiry, status, sTPhoneNo, dateOfBirth, company, routingNo, accountNo, accType, bankName, alternet;

    public int CustomerType { get; set; }
    public string TransactionId
    {
        get { return transactionId; }
        set { transactionId = value; }
    }

    public string InvoiceNo
    {
        get { return invoiceNo; }
        set { invoiceNo = value; }
    }

    public string Alternet
    {
        get { return alternet; }
        set { alternet = value; }
    }

    public string SCompany
    {
        get { return sCompany; }
        set { sCompany = value; }
    }

    public string OrderNo
    {
        get { return orderNo; }
        set { orderNo = value; }
    }

    public string RAN
    {
        get { return rAN; }
        set { rAN = value; }
    }

    public string OfficeNoS
    {
        get { return officeNoS; }
        set { officeNoS = value; }
    }

    public string OfficNoB
    {
        get { return officNoB; }
        set { officNoB = value; }
    }

    public string EcheckId
    {
        get { return echeckId; }
        set { echeckId = value; }
    }

    public string BankEntryId
    {
        get { return bankEntryId; }
        set { bankEntryId = value; }
    }


    public string BankName
    {
        get { return bankName; }
        set { bankName = value; }
    }

    public string AccType
    {
        get { return accType; }
        set { accType = value; }
    }

    public string AccountNo
    {
        get { return accountNo; }
        set { accountNo = value; }
    }

    public string RoutingNo
    {
        get { return routingNo; }
        set { routingNo = value; }
    }
    public string SySOrderNo
    {
        get;
        set;
    }
    public string Company
    {
        get { return company; }
        set { company = value; }
    }

    public bool DoNotMail
    {
        get { return doNotMail; }
        set { doNotMail = value; }
    }
    public bool DoNotEmail
    {
        get { return doNotEmail; }
        set { doNotEmail = value; }
    }
    public bool DoNotCall
    {
        get { return doNotCall; }
        set { doNotCall = value; }
    }
    public bool DoNotSMS
    {
        get { return doNotSMS; }
        set { doNotSMS = value; }
    }
    public string STphoneNo1
    {
        get { return sTphoneNo1; }
        set { sTphoneNo1 = value; }
    }
    public string STemailID
    {
        get { return sTemailID; }
        set { sTemailID = value; }
    }
    public string PhoneNo1
    {
        get { return phoneNo1; }
        set { phoneNo1 = value; }
    }
    public string SourceKey
    {
        get { return sourceKey; }
        set { sourceKey = value; }
    }
    public string DateOfBirth
    {
        get { return dateOfBirth; }
        set { dateOfBirth = value; }
    }
    public string STPhoneNo
    {
        get { return sTPhoneNo; }
        set { sTPhoneNo = value; }
    }
    public string BillEntryID
    {
        get { return billEntryID; }
        set { billEntryID = value; }
    }
    public string CreditEntryID
    {
        get { return creditEntryID; }
        set { creditEntryID = value; }
    }
    public string ShippingEntryID
    {
        get { return shippingEntryID; }
        set { shippingEntryID = value; }
    }
    public string Digit4
    {
        get { return digit4; }
        set { digit4 = value; }
    }
    public string CustID
    {
        get { return custID; }
        set { custID = value; }
    }
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }
    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }
    public string PhoneNo
    {
        get { return phoneNo; }
        set { phoneNo = value; }
    }
    public string EmailID
    {
        get { return emailID; }
        set { emailID = value; }
    }
    public string CustomerMedia
    {
        get { return customerMedia; }
        set { customerMedia = value; }
    }
    public string Status
    {
        get { return status; }
        set { status = value; }
    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public string Address1
    {
        get { return address1; }
        set { address1 = value; }
    }
    public string City
    {
        get { return city; }
        set { city = value; }
    }
    public string State
    {
        get { return state; }
        set { state = value; }
    }
    public string Country
    {
        get { return country; }
        set { country = value; }
    }
    public string Zip
    {
        get { return zip; }
        set { zip = value; }
    }
    public string CardType
    {
        get { return cardType; }
        set { cardType = value; }
    }
    public string NameOnCard
    {
        get { return nameOnCard; }
        set { nameOnCard = value; }
    }
    public string CardNo
    {
        get { return cardNo; }
        set { cardNo = value; }
    }
    public string CardExpiry
    {
        get { return cardExpiry; }
        set { cardExpiry = value; }
    }
    public string CvvNo
    {
        get { return cvvNo; }
        set { cvvNo = value; }
    }
    public string STFirstName
    {
        get { return sTFirstName; }
        set { sTFirstName = value; }
    }
    public string STLastName
    {
        get { return sTLastName; }
        set { sTLastName = value; }
    }
    public string STAddress
    {
        get { return sTAddress; }
        set { sTAddress = value; }
    }
    public string STAddress1
    {
        get { return sTAddress1; }
        set { sTAddress1 = value; }
    }
    public string STCity
    {
        get { return sTCity; }
        set { sTCity = value; }
    }
    public string STState
    {
        get { return sTState; }
        set { sTState = value; }
    }
    public string STCountry
    {
        get { return sTCountry; }
        set { sTCountry = value; }
    }
    public string STZip
    {
        get { return sTZip; }
        set { sTZip = value; }
    }

    public DateTime FromDate
    {
        get { if (fromdate == null || fromdate == DateTime.MinValue) { fromdate = Convert.ToDateTime("01/01/2010"); } return fromdate; }
        set { if (fromdate == null || fromdate == DateTime.MinValue) { fromdate = Convert.ToDateTime("01/01/2010"); } fromdate = value; }
    }

    public DateTime ToDate
    {
        get { if (todate == null || todate == DateTime.MinValue) { todate = Convert.ToDateTime("01/01/2010"); } return todate; }
        set { if (todate == null || todate == DateTime.MinValue) { todate = Convert.ToDateTime("01/01/2010"); } todate = value; }
    }
}