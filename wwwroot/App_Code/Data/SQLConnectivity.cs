﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SQLConnectivity
/// </summary>
public class SQLConnectivity
{
    SqlConnection sqlCon;

    public SqlConnection SqlCon
    {
        get { return sqlCon; }
        set { sqlCon = value; }
    }
    SqlCommand sqlCmd;

    public SqlCommand SqlCmd
    {
        get { return sqlCmd; }
        set { sqlCmd = value; }
    }
    SqlDataAdapter sqlAdp;

    public SqlDataAdapter SqlAdp
    {
        get { return sqlAdp; }
        set { sqlAdp = value; }
    }

    public SQLConnectivity()
    {
        sqlCon = new SqlConnection();
        SqlCon.ConnectionString = ConfigurationManager.ConnectionStrings["appConnectionString"].ConnectionString;
    }
}