﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DataCustomerDetail
/// </summary>
public class DataCustomerDetail
{
    public static void returnTable(PropertyCustomerDetail pobj)
    {
        DataBaseUtilitys dobj = new DataBaseUtilitys();
        SqlCommand sqlCmd = new SqlCommand("ProcCustomerDetail", dobj.SqlCon);
        sqlCmd.CommandTimeout = 500;
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;

        sqlCmd.Parameters.Add("@CustID", SqlDbType.NVarChar, 25).Value = pobj.CustID;
        sqlCmd.Parameters["@CustID"].Direction = ParameterDirection.InputOutput;

        sqlCmd.Parameters.Add("@DateOfBirth", SqlDbType.NVarChar).Value = pobj.DateOfBirth;
        sqlCmd.Parameters.Add("@Media", SqlDbType.NVarChar).Value = pobj.CustomerMedia;
        sqlCmd.Parameters.Add("@sourceKey", SqlDbType.NVarChar).Value = pobj.SourceKey;
        sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = pobj.Status;
        sqlCmd.Parameters.Add("@BillEntryID", SqlDbType.NVarChar, 25).Value = pobj.BillEntryID;
        sqlCmd.Parameters["@BillEntryID"].Direction = ParameterDirection.InputOutput;
        sqlCmd.Parameters.Add("@ShippingEntryID", SqlDbType.NVarChar, 25).Value = pobj.ShippingEntryID;
        sqlCmd.Parameters["@ShippingEntryID"].Direction = ParameterDirection.InputOutput;
        sqlCmd.Parameters.Add("@CreditEntryID", SqlDbType.NVarChar, 25).Value = pobj.CreditEntryID;
        sqlCmd.Parameters["@CreditEntryID"].Direction = ParameterDirection.InputOutput;
        sqlCmd.Parameters.Add("@DoNotMail", SqlDbType.Bit).Value = pobj.DoNotMail;
        sqlCmd.Parameters.Add("@DoNotEmail", SqlDbType.Bit).Value = pobj.DoNotEmail;
        sqlCmd.Parameters.Add("@DoNotCall", SqlDbType.Bit).Value = pobj.DoNotCall;
        sqlCmd.Parameters.Add("@DoNotSMS", SqlDbType.Bit).Value = pobj.DoNotSMS;

        sqlCmd.Parameters.Add("@CustomerType", SqlDbType.Int).Value = pobj.CustomerType;
        sqlCmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = pobj.FirstName;
        sqlCmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = pobj.LastName;
        sqlCmd.Parameters.Add("@Address", SqlDbType.NVarChar).Value = pobj.Address;
        sqlCmd.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = pobj.Address1;
        sqlCmd.Parameters.Add("@City", SqlDbType.NVarChar).Value = pobj.City;
        sqlCmd.Parameters.Add("@State", SqlDbType.NVarChar).Value = pobj.State;
        sqlCmd.Parameters.Add("@Country", SqlDbType.NVarChar).Value = pobj.Country;
        sqlCmd.Parameters.Add("@Zip", SqlDbType.NVarChar).Value = pobj.Zip;
        sqlCmd.Parameters.Add("@EmailID", SqlDbType.NVarChar).Value = pobj.EmailID;
        sqlCmd.Parameters.Add("@PhoneNo", SqlDbType.NVarChar).Value = pobj.PhoneNo;
        sqlCmd.Parameters.Add("@PhoneNo1", SqlDbType.NVarChar).Value = pobj.PhoneNo1;
        sqlCmd.Parameters.Add("@OfficeNoB", SqlDbType.NVarChar).Value = pobj.OfficNoB;
        sqlCmd.Parameters.Add("@OfficeNoS", SqlDbType.NVarChar).Value = pobj.OfficeNoS;
        sqlCmd.Parameters.Add("@SySOrderNo", SqlDbType.NVarChar).Value = pobj.SySOrderNo;

        sqlCmd.Parameters.Add("@STFirstName", SqlDbType.NVarChar).Value = pobj.STFirstName;
        sqlCmd.Parameters.Add("@STLastName", SqlDbType.NVarChar).Value = pobj.STLastName;
        sqlCmd.Parameters.Add("@STAddress", SqlDbType.NVarChar).Value = pobj.STAddress;
        sqlCmd.Parameters.Add("@STAddress1", SqlDbType.NVarChar).Value = pobj.STAddress1;
        sqlCmd.Parameters.Add("@STCity", SqlDbType.NVarChar).Value = pobj.STCity;
        sqlCmd.Parameters.Add("@STState", SqlDbType.NVarChar).Value = pobj.STState;
        sqlCmd.Parameters.Add("@STCountry", SqlDbType.NVarChar).Value = pobj.STCountry;
        sqlCmd.Parameters.Add("@STZip", SqlDbType.NVarChar).Value = pobj.STZip;
        sqlCmd.Parameters.Add("@STPhoneNo", SqlDbType.NVarChar).Value = pobj.STPhoneNo;
        sqlCmd.Parameters.Add("@STPhoneNo1", SqlDbType.NVarChar).Value = pobj.STphoneNo1;
        sqlCmd.Parameters.Add("@STEmailID", SqlDbType.NVarChar).Value = pobj.STemailID;
        sqlCmd.Parameters.Add("@STCompany", SqlDbType.NVarChar).Value = pobj.SCompany;
        sqlCmd.Parameters.Add("@Company", SqlDbType.NVarChar).Value = pobj.Company;

        sqlCmd.Parameters.Add("@CardType", SqlDbType.NVarChar).Value = pobj.CardType;
        sqlCmd.Parameters.Add("@NameOnCard", SqlDbType.NVarChar).Value = pobj.NameOnCard;
        sqlCmd.Parameters.Add("@CardNo", SqlDbType.NVarChar).Value = pobj.CardNo;
        sqlCmd.Parameters.Add("@CardExpiry", SqlDbType.NVarChar).Value = pobj.CardExpiry;
        sqlCmd.Parameters.Add("@CvvNo", SqlDbType.NVarChar).Value = pobj.CvvNo;
        sqlCmd.Parameters.Add("@Digit4", SqlDbType.NVarChar).Value = pobj.Digit4;

        sqlCmd.Parameters.Add("@AccountNo", SqlDbType.VarChar).Value = pobj.AccountNo;
        sqlCmd.Parameters.Add("@RoutingNo", SqlDbType.VarChar).Value = pobj.RoutingNo;
        sqlCmd.Parameters.Add("@AccType", SqlDbType.NVarChar).Value = pobj.AccType;
        sqlCmd.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = pobj.BankName;
        sqlCmd.Parameters.Add("@BankEntryId", SqlDbType.NVarChar).Value = pobj.BankEntryId;
        sqlCmd.Parameters.Add("@EcheckId", SqlDbType.NVarChar, 50).Value = pobj.EcheckId;
        sqlCmd.Parameters.Add("@Alternet", SqlDbType.NVarChar).Value = pobj.Alternet;
        sqlCmd.Parameters.Add("@TokenNo", SqlDbType.NVarChar).Value = pobj.TokenNo;
        sqlCmd.Parameters["@EcheckId"].Direction = ParameterDirection.InputOutput;

        sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;
        sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;

        sqlCmd.Parameters.Add("@InvoiceNo", SqlDbType.NVarChar, 50).Value = pobj.InvoiceNo;
        sqlCmd.Parameters.Add("@TransactionId", SqlDbType.NVarChar, 50).Value = pobj.TransactionId;
        if (pobj.FromDate > Convert.ToDateTime("01/01/2010"))
        {
            sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;
        }
        if (pobj.ToDate > Convert.ToDateTime("01/01/2010"))
        {
            sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;
        }

        sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
        sqlCmd.Parameters.Add("@RAN", SqlDbType.NVarChar, 50).Value = pobj.RAN;

        sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
        sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

        sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
        sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

        //SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
        //pobj.Dt = new DataTable();
        //sqlAdp.Fill(pobj.Dt);
        pobj.ds = new DataSet();
        sqlCmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = sqlCmd;
        da.Fill(pobj.ds);
        if (pobj.ds != null && pobj.ds.Tables.Count != 0)
        {
            pobj.Dt = pobj.ds.Tables[0];
        }
      
        pobj.CustID = sqlCmd.Parameters["@CustID"].Value.ToString();
        pobj.BillEntryID = sqlCmd.Parameters["@BillEntryID"].Value.ToString();
        pobj.ShippingEntryID = sqlCmd.Parameters["@ShippingEntryID"].Value.ToString();
        pobj.CreditEntryID = sqlCmd.Parameters["@CreditEntryID"].Value.ToString();
        pobj.EcheckId = sqlCmd.Parameters["@EcheckId"].Value.ToString();
        pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
        pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
    }

}