﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DataLogin
/// </summary>
public class DataLogin
{
    public static void returnTable(PropertyLogin pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("Proc_Customer_Login", dobj.SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = 1000;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.Add("@email", SqlDbType.NVarChar, 50).Value = pobj.EmailID;
            sqlCmd.Parameters.Add("@zip", SqlDbType.NVarChar, 50).Value = pobj.Zipcode;
            sqlCmd.Parameters.Add("@IpAddress", SqlDbType.NVarChar, 50).Value = pobj.IpAddress;
            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
            pobj.Dt = new DataTable();
            sqlAdp.Fill(pobj.Dt);
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception e)
        { }
    }
 }