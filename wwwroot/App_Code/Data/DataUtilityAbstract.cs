﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DataUtilityAbstract
/// </summary>
public interface DataUtilityAbstract
{
     void executeNonQuery(object obj);
     void returnDataTable(object obj);
     void returnScalarString(object obj);
     void returnScalarInteger(object obj);
     void returnScalarDecimal(object obj);
}