﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

/// <summary>
/// Summary description for DataProcessImportOrders
/// </summary>
public class DataProcessImportOrders
{
    public static void returnTable(PropertyProcessImportOrders pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("ProcProcessImportOrders", dobj.SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = 10000;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;

            sqlCmd.Parameters.Add("@CustID", SqlDbType.NVarChar, 50).Value = pobj.CustID;
            sqlCmd.Parameters["@CustID"].Direction = ParameterDirection.InputOutput;
            
            sqlCmd.Parameters.Add("@CreditEntryID", SqlDbType.NVarChar, 50).Value = pobj.CreditEntryID;
            sqlCmd.Parameters["@CreditEntryID"].Direction = ParameterDirection.InputOutput;

            sqlCmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = pobj.LastName;
            sqlCmd.Parameters.Add("@SkU", SqlDbType.VarChar).Value = pobj.SkU;
            sqlCmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = pobj.FirstName;
            sqlCmd.Parameters.Add("@DateOfBirth", SqlDbType.NVarChar).Value = pobj.DateOfBirth;
            sqlCmd.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = pobj.Address1;
            sqlCmd.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = pobj.Address2;
            sqlCmd.Parameters.Add("@City", SqlDbType.NVarChar).Value = pobj.City;
            sqlCmd.Parameters.Add("@State", SqlDbType.NVarChar).Value = pobj.State;
            sqlCmd.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = pobj.ZipCode;
            sqlCmd.Parameters.Add("@Country", SqlDbType.NVarChar).Value = pobj.Country;
            sqlCmd.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = pobj.Phone1;
            sqlCmd.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = pobj.Phone2;
            sqlCmd.Parameters.Add("@EmailID", SqlDbType.NVarChar).Value = pobj.EmailID;
            sqlCmd.Parameters.Add("@CardType", SqlDbType.NVarChar).Value = pobj.CardType;
            sqlCmd.Parameters.Add("@CardNum", SqlDbType.NVarChar, 200).Value = pobj.CardNum;
            sqlCmd.Parameters.Add("@Digit4", SqlDbType.NVarChar).Value = pobj.Digit4;
            sqlCmd.Parameters.Add("@Expires", SqlDbType.NVarChar, 15).Value = pobj.Expires;
            sqlCmd.Parameters.Add("@SFirstName", SqlDbType.NVarChar).Value = pobj.SFirstName;
            sqlCmd.Parameters.Add("@SLastName", SqlDbType.NVarChar).Value = pobj.SLastName;
            sqlCmd.Parameters.Add("@SAddress1", SqlDbType.NVarChar).Value = pobj.SAddress1;
            sqlCmd.Parameters.Add("@SAddress2", SqlDbType.NVarChar).Value = pobj.SAddress2;
            sqlCmd.Parameters.Add("@SCity", SqlDbType.NVarChar).Value = pobj.SCity;
            sqlCmd.Parameters.Add("@SState", SqlDbType.NVarChar).Value = pobj.SState;
            sqlCmd.Parameters.Add("@SZip", SqlDbType.NVarChar).Value = pobj.SZip;
            sqlCmd.Parameters.Add("@SCountry", SqlDbType.NVarChar).Value = pobj.SCountry;
            sqlCmd.Parameters.Add("@SPhone", SqlDbType.NVarChar).Value = pobj.SPhone1;
            sqlCmd.Parameters.Add("@SPhone1", SqlDbType.NVarChar).Value = pobj.SPhone2;
            sqlCmd.Parameters.Add("@SEmailID", SqlDbType.NVarChar).Value = pobj.SEmailID;
            sqlCmd.Parameters.Add("@Product01", SqlDbType.NVarChar).Value = pobj.Product01;
            sqlCmd.Parameters.Add("@Quantity01", SqlDbType.Decimal).Value = pobj.Quantity01;
            sqlCmd.Parameters.Add("@Product02", SqlDbType.NVarChar).Value = pobj.Product02;
            sqlCmd.Parameters.Add("@Quantity02", SqlDbType.Decimal).Value = pobj.Quantity02;
            sqlCmd.Parameters.Add("@Product03", SqlDbType.NVarChar).Value = pobj.Product03;
            sqlCmd.Parameters.Add("@Quantity03", SqlDbType.Decimal).Value = pobj.Quantity03;
            sqlCmd.Parameters.Add("@Product04", SqlDbType.NVarChar).Value = pobj.Product04;
            sqlCmd.Parameters.Add("@Quantity04", SqlDbType.Decimal).Value = pobj.Quantity04;
            sqlCmd.Parameters.Add("@Product05", SqlDbType.NVarChar).Value = pobj.Product05;
            sqlCmd.Parameters.Add("@Quantity05", SqlDbType.Decimal).Value = pobj.Quantity05;
            sqlCmd.Parameters.Add("@Product06", SqlDbType.NVarChar).Value = pobj.Product06;
            sqlCmd.Parameters.Add("@Quantity06", SqlDbType.Decimal).Value = pobj.Quantity06;
            sqlCmd.Parameters.Add("@Product07", SqlDbType.NVarChar).Value = pobj.Product07;
            sqlCmd.Parameters.Add("@Quantity07", SqlDbType.Decimal).Value = pobj.Quantity07;
            sqlCmd.Parameters.Add("@Product08", SqlDbType.NVarChar).Value = pobj.Product08;
            sqlCmd.Parameters.Add("@Quantity08", SqlDbType.Decimal).Value = pobj.Quantity08;
            sqlCmd.Parameters.Add("@Product09", SqlDbType.NVarChar).Value = pobj.Product09;
            sqlCmd.Parameters.Add("@Quantity09", SqlDbType.Decimal).Value = pobj.Quantity09;
            sqlCmd.Parameters.Add("@Product10", SqlDbType.NVarChar).Value = pobj.Product10;
            sqlCmd.Parameters.Add("@Quantity10", SqlDbType.Decimal).Value = pobj.Quantity10;
            sqlCmd.Parameters.Add("@Continued", SqlDbType.NVarChar).Value = pobj.Continued;
            sqlCmd.Parameters.Add("@HappyPoints", SqlDbType.Decimal).Value = pobj.HappyPoints;
            sqlCmd.Parameters.Add("@UsePrices", SqlDbType.Bit).Value = pobj.UsePrice;
            sqlCmd.Parameters.Add("@Price01", SqlDbType.Decimal).Value = pobj.Price01;
            sqlCmd.Parameters.Add("@Discount01", SqlDbType.Decimal).Value = pobj.Discount01;
            sqlCmd.Parameters.Add("@Price02", SqlDbType.Decimal).Value = pobj.Price02;
            sqlCmd.Parameters.Add("@Discount02", SqlDbType.Decimal).Value = pobj.Discount02;
            sqlCmd.Parameters.Add("@Price03", SqlDbType.Decimal).Value = pobj.Price03;
            sqlCmd.Parameters.Add("@Discount03", SqlDbType.Decimal).Value = pobj.Discount03;
            sqlCmd.Parameters.Add("@Price04", SqlDbType.Decimal).Value = pobj.Price04;
            sqlCmd.Parameters.Add("@Discount04", SqlDbType.Decimal).Value = pobj.Discount04;
            sqlCmd.Parameters.Add("@Price05", SqlDbType.Decimal).Value = pobj.Price05;
            sqlCmd.Parameters.Add("@Discount05", SqlDbType.Decimal).Value = pobj.Discount05;
            sqlCmd.Parameters.Add("@Price06", SqlDbType.Decimal).Value = pobj.Price06;
            sqlCmd.Parameters.Add("@Discount06", SqlDbType.Decimal).Value = pobj.Discount06;
            sqlCmd.Parameters.Add("@Price07", SqlDbType.Decimal).Value = pobj.Price07;
            sqlCmd.Parameters.Add("@Discount07", SqlDbType.Decimal).Value = pobj.Discount07;
            sqlCmd.Parameters.Add("@Price08", SqlDbType.Decimal).Value = pobj.Price08;
            sqlCmd.Parameters.Add("@Discount08", SqlDbType.Decimal).Value = pobj.Discount08;
            sqlCmd.Parameters.Add("@Price09", SqlDbType.Decimal).Value = pobj.Price09;
            sqlCmd.Parameters.Add("@Discount09", SqlDbType.Decimal).Value = pobj.Discount09;
            sqlCmd.Parameters.Add("@Price10", SqlDbType.Decimal).Value = pobj.Price10;
            sqlCmd.Parameters.Add("@Discount10", SqlDbType.Decimal).Value = pobj.Discount10;
            sqlCmd.Parameters.Add("@Credit", SqlDbType.Decimal).Value = pobj.Credit;

            sqlCmd.Parameters.Add("@Greetings", SqlDbType.NVarChar, 100).Value = pobj.Greetings;

            sqlCmd.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar).Value = pobj.PaymentMethod;
            sqlCmd.Parameters.Add("@CustNum", SqlDbType.NVarChar).Value = pobj.CustNum;
            sqlCmd.Parameters.Add("@AltNum", SqlDbType.NVarChar).Value = pobj.AltNum;
            sqlCmd.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = pobj.Comment;
            sqlCmd.Parameters.Add("@SourceKey", SqlDbType.NVarChar).Value = pobj.SourceKey;
            sqlCmd.Parameters.Add("@SalesID", SqlDbType.NVarChar).Value = pobj.SalesID;
            sqlCmd.Parameters.Add("@ShipVIA", SqlDbType.NVarChar).Value = pobj.ShipVIA;
            sqlCmd.Parameters.Add("@Paid", SqlDbType.Decimal).Value = pobj.Paid;
            sqlCmd.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = pobj.OrderDate;
            sqlCmd.Parameters.Add("@OrderHoldDate", SqlDbType.DateTime).Value = pobj.OrderHoldDate;
            sqlCmd.Parameters.Add("@OrderNote", SqlDbType.NVarChar).Value = pobj.OrderNote;

            sqlCmd.Parameters.Add("@MoneyOrder", SqlDbType.NVarChar).Value = pobj.MoneyOrdNo;
            sqlCmd.Parameters.Add("@CardHolder", SqlDbType.NVarChar).Value = pobj.CardHolder;
            sqlCmd.Parameters.Add("@RoutingNum", SqlDbType.NVarChar).Value = pobj.RoutingNum;
            sqlCmd.Parameters.Add("@AccountNum", SqlDbType.NVarChar).Value = pobj.AccountNum;
            sqlCmd.Parameters.Add("@AccountType", SqlDbType.NVarChar).Value = pobj.AccountType;
            sqlCmd.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = pobj.BankName;

            sqlCmd.Parameters.Add("@MediaCode", SqlDbType.NVarChar).Value = pobj.MediaCode;
            sqlCmd.Parameters.Add("@DNIS", SqlDbType.NVarChar).Value = pobj.DNIS;
            sqlCmd.Parameters.Add("@Shipping", SqlDbType.Decimal).Value = pobj.Shipping;
            sqlCmd.Parameters.Add("@UseShipping", SqlDbType.Bit).Value = pobj.UseShipping;
            sqlCmd.Parameters.Add("@UseShippingcheck", SqlDbType.VarChar).Value = pobj.UseShippingcheck;
            sqlCmd.Parameters.Add("@SYSOrderNo", SqlDbType.NVarChar).Value = pobj.SYSOrderNo;
            sqlCmd.Parameters.Add("@OrderProcessingDate", SqlDbType.DateTime).Value = pobj.OrderProcessingDate;
            sqlCmd.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = pobj.TransactionID;
            sqlCmd.Parameters.Add("@OrderStatus", SqlDbType.NVarChar).Value = pobj.OrderStatus;
            sqlCmd.Parameters.Add("@OrderType", SqlDbType.NVarChar).Value = pobj.OrderType;
            sqlCmd.Parameters.Add("@CheckNo", SqlDbType.VarChar).Value = pobj.CheckNo;
            sqlCmd.Parameters.Add("@SubTotal", SqlDbType.Decimal).Value = pobj.SubTotal;
            sqlCmd.Parameters.AddWithValue("@Overalldiscount", pobj.Overalldiscount);
            sqlCmd.Parameters.AddWithValue("@coupancode", pobj.Coupancode);
            sqlCmd.Parameters.AddWithValue("@coupanamount", pobj.Coupanamount);
            sqlCmd.Parameters.AddWithValue("@Approvecode", pobj.Approvecode);
            sqlCmd.Parameters.AddWithValue("@Createdby", pobj.Createdby);
            sqlCmd.Parameters.AddWithValue("@CustomerPaymentProfileId", pobj.CustomerPaymentProfileId);
            sqlCmd.Parameters.AddWithValue("@CustomerProfileId", pobj.CustomerProfileId);
            sqlCmd.Parameters.AddWithValue("@PaymentGateway", pobj.PaymentGateway);
            sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
            sqlCmd.Parameters.AddWithValue("@CategoryId", pobj.CategoryId);
            sqlCmd.Parameters.AddWithValue("@HearAboutUs", pobj.HearAboutUs);
            sqlCmd.Parameters.AddWithValue("@OrderOriginalSource", pobj.OrderOriginalSource);
            sqlCmd.Parameters.AddWithValue("@Tax", pobj.Tax);
            #region Show Cancel Button
            if (pobj.OpCode == 21 || pobj.OpCode == 23)
            {
                //sqlCmd.Parameters.Add("@CancelDate", SqlDbType.DateTime).Value = pobj.CancelDate;
                sqlCmd.Parameters.Add("@CancelDesc", SqlDbType.NVarChar).Value = pobj.CancelDesc;
            }
            //if (pobj.OpCode == 21)
            //{
            //    //sqlCmd.Parameters.Add("@CancelDate", SqlDbType.DateTime).Value = pobj.CancelDate;
            //    sqlCmd.Parameters.Add("@CancelDesc", SqlDbType.NVarChar).Value = pobj.CancelDesc;
            //}
            #endregion


            if (pobj.OpCode == 61 || pobj.OpCode == 62 || pobj.OpCode == 64 || pobj.OpCode == 68 || pobj.OpCode == 69 || pobj.OpCode == 70 || pobj.OpCode == 75 || pobj.OpCode == 71)
            {
                sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;
                sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;

            }
            if (pobj.OpCode == 61)
            {
                sqlCmd.Parameters.Add("@ToHoldDate", SqlDbType.DateTime).Value = pobj.ToHoldDate;
                sqlCmd.Parameters.Add("@FromHoldDate", SqlDbType.DateTime).Value = pobj.FromHoldDate;
            }
            if (pobj.OpCode == 46)
            {
                sqlCmd.Parameters.Add("@TodayDate", SqlDbType.DateTime).Value = pobj.CreationDate;
            }
            sqlCmd.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = pobj.OrderID;
            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            #region Show Cancel Button
            sqlCmd.Parameters.Add("@InvoiceNo", SqlDbType.NVarChar).Value = pobj.InvoiceNo;
            #endregion

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.ds = new DataSet();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.ds);
            if (pobj.ds != null && pobj.ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.ds.Tables[0];
            }
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            pobj.CustID = sqlCmd.Parameters["@CustID"].Value.ToString();
            pobj.CreditEntryID = sqlCmd.Parameters["@CreditEntryID"].Value.ToString();
        }
        catch(Exception ex)
        { 
        
        }
    }
    
   
 }