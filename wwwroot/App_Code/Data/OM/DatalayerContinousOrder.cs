﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DatalayerContinousOrder
/// </summary>
public class DatalayerContinousOrder
{
	
    public static void returnTable(PropertyLayerConitnousOrder pobj)
    {
        DataBaseUtilitys dobj = new DataBaseUtilitys();
        SqlCommand sqlCmd = new SqlCommand("Proc_ContinousOrderLog", dobj.SqlCon);
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.CommandTimeout = 1000;
        sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
        if(pobj.OpCode!=52)
        {
        sqlCmd.Parameters.Add("@InvoiceNo", SqlDbType.NVarChar, 25).Value = pobj.InvoiceNo;
        sqlCmd.Parameters.Add("@LastInvoiceNo", SqlDbType.NVarChar, 25).Value = pobj.LastInvoiceNo;
        sqlCmd.Parameters.Add("@SYSorderno", SqlDbType.NVarChar, 25).Value = pobj.SYSorderno;
        sqlCmd.Parameters.Add("@SYSordernoNew", SqlDbType.NVarChar, 25).Value = pobj.SYSordernoNew;
        sqlCmd.Parameters.Add("@BillingEntryID", SqlDbType.NVarChar, 25).Value = pobj.BillingEntryID;
        sqlCmd.Parameters.Add("@ShippingEntryID", SqlDbType.NVarChar, 25).Value = pobj.ShippingEntryID;
        sqlCmd.Parameters.Add("@CreditEntryID", SqlDbType.NVarChar, 25).Value = pobj.CreditEntryID;
        sqlCmd.Parameters.Add("@CustID", SqlDbType.NVarChar, 50).Value = pobj.CustID;
        sqlCmd.Parameters.Add("@Custname", SqlDbType.NVarChar, 50).Value = pobj.CustName;
        
        sqlCmd.Parameters.Add("@ReshipNo", SqlDbType.Int, 50).Value = pobj.ReShippingNo;
        sqlCmd.Parameters.Add("@ShipInterval", SqlDbType.Int, 50).Value = pobj.ShippingInterval;
        sqlCmd.Parameters.Add("@DeclineCount", SqlDbType.Int, 50).Value = pobj.Declinecount;
        
        sqlCmd.Parameters.Add("@OrderType", SqlDbType.NVarChar, 50).Value = pobj.OrderType;
        sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar, 50).Value = pobj.Status;
       
        sqlCmd.Parameters.Add("@UnitPrice", SqlDbType.Decimal, 100).Value = pobj.UnitPrice;
        sqlCmd.Parameters.Add("@SPUnitPrice", SqlDbType.Decimal, 100).Value = pobj.SPUnitPrice;
        sqlCmd.Parameters.Add("@PInvoiceType", SqlDbType.NVarChar, 50).Value = pobj.ParentInvoiceType;

        sqlCmd.Parameters.AddWithValue("@SKUXml", pobj.SKUXml);
        sqlCmd.Parameters.AddWithValue("@SKuProductXml", pobj.SKuProductXml);
        sqlCmd.Parameters.AddWithValue("@CreditGiven", pobj.CreditGiven);
        sqlCmd.Parameters.AddWithValue("@HappyPoints", pobj.HappyPoints);
        sqlCmd.Parameters.AddWithValue("@AHappyPointAmount", pobj.AHappyPointAmount);
        sqlCmd.Parameters.AddWithValue("@Subtotal", pobj.Subtotal);
        sqlCmd.Parameters.AddWithValue("@StateTax", pobj.StateTax);
        sqlCmd.Parameters.AddWithValue("@OverallDiscount", pobj.OverallDiscount);
        sqlCmd.Parameters.AddWithValue("@Shipping", pobj.Shipping);
        sqlCmd.Parameters.AddWithValue("@SKUType", pobj.SKUType);
        if (pobj.TempHoldDate == "" || pobj.TempHoldDate == null)
            sqlCmd.Parameters.Add("@TempHoldDate", SqlDbType.DateTime).Value = DBNull.Value;
        else
            sqlCmd.Parameters.Add("@TempHoldDate", SqlDbType.DateTime).Value = pobj.TempHoldDate;
        sqlCmd.Parameters.Add("@Coments", SqlDbType.NVarChar, 500).Value = pobj.Comments;
        sqlCmd.Parameters.Add("@CustStatus", SqlDbType.NVarChar, 25).Value = pobj.CustStatus;
        sqlCmd.Parameters.Add("@ProductId", SqlDbType.NVarChar, 25).Value = pobj.ProductId;
        sqlCmd.Parameters.Add("@Sourcekey", SqlDbType.NVarChar).Value = pobj.SourceKeyId;
        sqlCmd.Parameters.Add("@SkuID", SqlDbType.NVarChar, 25).Value = pobj.SkuID;
        sqlCmd.Parameters.Add("@Quantity", SqlDbType.Decimal, 18).Value = pobj.Quantity;
        sqlCmd.Parameters.Add("@SPQuantity", SqlDbType.Decimal, 18).Value = pobj.SPQuantity;
        sqlCmd.Parameters.Add("@Discount", SqlDbType.Decimal, 18).Value = pobj.Discount;
        sqlCmd.Parameters.Add("@SPDiscount", SqlDbType.Decimal, 18).Value = pobj.SPDiscount;
        sqlCmd.Parameters.Add("@onetimeonly", SqlDbType.Bit).Value = pobj.onetimeonly;
        }
        if (pobj.OpCode == 11 ||  pobj.OpCode == 22 || pobj.OpCode == 12 || pobj.OpCode==25) 
        {
            sqlCmd.Parameters.Add("@NxtShippingDate", SqlDbType.DateTime, 50).Value = pobj.NxtshippingDate;
            sqlCmd.Parameters.Add("@PrevShippingDate", SqlDbType.DateTime, 50).Value = pobj.PrevshippingDate;
        }
        if (pobj.OpCode == 55)
        {
            sqlCmd.Parameters.Add("@ToNxtShpdate", SqlDbType.DateTime, 50).Value = pobj.ToNxtShpdate;
            sqlCmd.Parameters.Add("@FromNxtShpdate", SqlDbType.DateTime, 50).Value = pobj.FromNxtShpdate;
            sqlCmd.Parameters.Add("@ToLstShpdate", SqlDbType.DateTime, 50).Value = pobj.ToLstShpdate;
            sqlCmd.Parameters.Add("@FromLstShpdate", SqlDbType.DateTime, 50).Value = pobj.FromLstShpdate;
        }
        sqlCmd.Parameters.Add("@PaymentID", SqlDbType.NVarChar, 50).Value = pobj.PaymentID;
        sqlCmd.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 50).Value = pobj.PaymentMethod;


        sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
        sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;
        sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;

        sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
        sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

        sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
        sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

        if (pobj.OpCode == 81)
        {
            sqlCmd.Parameters.Add("@lastdeclineDate", SqlDbType.DateTime, 50).Value = pobj.LastDeclineDate;
            sqlCmd.Parameters.Add("@CancelDate", SqlDbType.DateTime, 50).Value = pobj.CancelDate;

        }


        pobj.ds = new DataSet();
        sqlCmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = sqlCmd;
        da.Fill(pobj.ds);
        if (pobj.ds != null && pobj.ds.Tables.Count != 0)
        {
            pobj.Dt = pobj.ds.Tables[0];
        }
        
        pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
        pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
    }
}