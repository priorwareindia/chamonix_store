﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

/// <summary>
/// Summary description for DataInvoiceMaster
/// </summary>
public class DataInvoiceMaster
{
    public static void returnTable(PropertyInvoiceMaster pobj)
    {
        try
        {
            DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new SqlCommand("ProcInvoiceMaster", dobj.SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = 100000;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;

            sqlCmd.Parameters.Add("@InvoiceNo", SqlDbType.NVarChar,10000).Value = pobj.InvoiceNo;
            sqlCmd.Parameters["@InvoiceNo"].Direction = ParameterDirection.InputOutput;

            sqlCmd.Parameters.Add("@SYSorderno", SqlDbType.NVarChar).Value = pobj.SYSorderno;
            sqlCmd.Parameters.Add("@BillEntryID", SqlDbType.NVarChar).Value = pobj.BillingEntryID;
            sqlCmd.Parameters.Add("@ShippingEntryID", SqlDbType.NVarChar).Value = pobj.ShippingEntryID;
            sqlCmd.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = pobj.PaymentID;
            sqlCmd.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar).Value = pobj.PaymentMethod;
            sqlCmd.Parameters.Add("@CustID", SqlDbType.NVarChar).Value = pobj.CustID;
            sqlCmd.Parameters.Add("@ShipVIA", SqlDbType.NVarChar).Value = pobj.ShipVIA;
            sqlCmd.Parameters.Add("@SKUXml", SqlDbType.NVarChar).Value = pobj.SKUXml;
            sqlCmd.Parameters.Add("@SKuProductXml", SqlDbType.NVarChar).Value = pobj.SKuProductXml;
            sqlCmd.Parameters.AddWithValue("@CustInvoiceType", pobj.CustInvoiceType);
            sqlCmd.Parameters.AddWithValue("@AutoshipType", pobj.AutoshipType);
            sqlCmd.Parameters.AddWithValue("@HearAboutUs", pobj.HearAboutUs);
            sqlCmd.Parameters.Add("@ShippingDate", SqlDbType.NVarChar).Value = pobj.ShippingDate;
            sqlCmd.Parameters.Add("@OrderMemo", SqlDbType.VarChar).Value = pobj.OrderMemo;
            sqlCmd.Parameters.Add("@TrackingNo", SqlDbType.NVarChar).Value = pobj.TrackingNo;
            sqlCmd.Parameters.Add("@BarcodeNo", SqlDbType.NVarChar).Value = pobj.BarcodeNo;
            sqlCmd.Parameters.Add("@OrderType", SqlDbType.NVarChar).Value = pobj.OrderType;
            sqlCmd.Parameters.Add("@InvoiceStatus", SqlDbType.NVarChar).Value = pobj.InvoiceStatus;
            sqlCmd.Parameters.Add("@CancelDesc", SqlDbType.NVarChar).Value = pobj.CancelDesc;
            sqlCmd.Parameters.Add("@CancelInvoiceNo", SqlDbType.NVarChar).Value = pobj.CancelInvoiceNo;
            sqlCmd.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = pobj.TransactionID;
            sqlCmd.Parameters.Add("@RefundAgainstTransactionID", SqlDbType.NVarChar).Value = pobj.RefundAgainstTransactionID;
            sqlCmd.Parameters.Add("@ShippingType", SqlDbType.NVarChar).Value = pobj.ShippingType;

            sqlCmd.Parameters.Add("@ShippingInterval", SqlDbType.Int).Value = pobj.ShippingInterval;
            sqlCmd.Parameters.Add("@Invnote", SqlDbType.NVarChar).Value = pobj.Invnote;
            sqlCmd.Parameters.Add("@InvoiceType", SqlDbType.NVarChar).Value = pobj.InvoiceType;
            sqlCmd.Parameters.Add("@SalesID", SqlDbType.NVarChar).Value = pobj.SalesID;
            sqlCmd.Parameters.Add("@Terms", SqlDbType.NVarChar).Value = pobj.Terms;
            sqlCmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = pobj.FirstName;
            if (pobj.Subtotalamt != "" && pobj.Subtotalamt != null)
            {
                sqlCmd.Parameters.Add("@Subtotalamt", SqlDbType.Decimal).Value = pobj.Subtotalamt;
            }
            else
            {
                sqlCmd.Parameters.Add("@Subtotalamt", SqlDbType.Decimal).Value = 0;
            }
            if (pobj.Shipamt != "" && pobj.Shipamt != null)
            {
                sqlCmd.Parameters.Add("@Shipamt", SqlDbType.Decimal).Value = pobj.Shipamt;
            }
            else
            {
                sqlCmd.Parameters.Add("@Shipamt", SqlDbType.Decimal).Value = 0;
            }


            sqlCmd.Parameters.Add("@SourceKey", SqlDbType.NVarChar).Value = pobj.SourceKey;
            sqlCmd.Parameters.Add("@SaleSource", SqlDbType.NVarChar).Value = pobj.SaleSource;
            if (pobj.CountiStatus != null)
            {
                sqlCmd.Parameters.Add("@CountiStatus", SqlDbType.Int).Value = pobj.CountiStatus;

            }
            else
                sqlCmd.Parameters.Add("@CountiStatus", SqlDbType.Int).Value = 1;
            if (pobj.Taxamt != "" && pobj.Taxamt != null)
            {
                sqlCmd.Parameters.Add("@Taxamt", SqlDbType.Decimal).Value = pobj.Taxamt;
            }
            else {
                sqlCmd.Parameters.Add("@Taxamt", SqlDbType.Decimal).Value = 0;
            }

            sqlCmd.Parameters.Add("@TaxableAmount", SqlDbType.Decimal).Value = pobj.TaxableAmount;
           
            if (pobj.BillTotalamt != "" && pobj.BillTotalamt != null)
            {
                sqlCmd.Parameters.Add("@BillTotalamt", SqlDbType.Decimal).Value = pobj.BillTotalamt;
            }
            else {
                sqlCmd.Parameters.Add("@BillTotalamt", SqlDbType.Decimal).Value = 0;
            }
            if (pobj.StateTax != "" && pobj.StateTax != null)
            {
                sqlCmd.Parameters.Add("@StateTax", SqlDbType.Decimal).Value = pobj.StateTax;
            }
            else
            {
                sqlCmd.Parameters.Add("@StateTax", SqlDbType.Decimal).Value = 0;
            }
            
            sqlCmd.Parameters.Add("@DiscountPer", SqlDbType.Decimal).Value = pobj.DiscountPer;
            if (pobj.DiscountAmt != "" && pobj.DiscountAmt != null)
            {
                sqlCmd.Parameters.Add("@DiscountAmt", SqlDbType.Decimal).Value = pobj.DiscountAmt;
            }
            else
            {
                sqlCmd.Parameters.Add("@DiscountAmt", SqlDbType.Decimal).Value = 0;
            }
            
            if (pobj.ShippingQty != "" && pobj.ShippingQty != null)
            {
                sqlCmd.Parameters.Add("@ShippingQty", SqlDbType.Decimal).Value = pobj.ShippingQty;
            }
            else
            {
                sqlCmd.Parameters.Add("@ShippingQty", SqlDbType.Decimal).Value = 0;
            }

            sqlCmd.Parameters.Add("@AccountNo", SqlDbType.VarChar).Value = pobj.AccountNo;
            sqlCmd.Parameters.Add("@RoutingNo", SqlDbType.VarChar).Value = pobj.RoutingNo;

            sqlCmd.Parameters.Add("@CheckNo", SqlDbType.VarChar).Value = pobj.CheckNo;
            if (pobj.Shippnigcost != "" && pobj.Shippnigcost != null)
            {
                sqlCmd.Parameters.Add("@Shippigcost", SqlDbType.Decimal).Value = pobj.Shippnigcost;
            }
            else
            {
                sqlCmd.Parameters.Add("@Shippigcost", SqlDbType.Decimal).Value = 0;
            }
            
            sqlCmd.Parameters.Add("@Credit", SqlDbType.Decimal).Value = pobj.Credit;
            sqlCmd.Parameters.Add("@OrderStatus", SqlDbType.NVarChar).Value = pobj.OrderStatus;
            sqlCmd.Parameters.Add("@UsePrice", SqlDbType.Bit).Value = pobj.UsePrice;
            sqlCmd.Parameters.Add("@ReferenceNo", SqlDbType.NVarChar).Value = pobj.ReferenceNo;
            sqlCmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = pobj.Price;
            sqlCmd.Parameters.Add("@SPUnitPrice", SqlDbType.Decimal).Value = pobj.SPUnitPrice;
            sqlCmd.Parameters.Add("@ActualCharge", SqlDbType.Decimal).Value = pobj.ActualCharged;
            sqlCmd.Parameters.Add("@BackOrder", SqlDbType.Bit).Value = pobj.BackOrder;
            sqlCmd.Parameters.Add("@NoOfHappyPts", SqlDbType.Decimal).Value = pobj.NoOfHappyPts;
            sqlCmd.Parameters.Add("@ApproveCode", SqlDbType.NVarChar).Value = pobj.ApproveCode;
            sqlCmd.Parameters.Add("@PaymentType", SqlDbType.NVarChar).Value = pobj.PaymentType;
            sqlCmd.Parameters.Add("@TransactionType", SqlDbType.NVarChar).Value = pobj.TransactionType;
            sqlCmd.Parameters.Add("@Amount", SqlDbType.Decimal).Value = pobj.Amount;
            sqlCmd.Parameters.Add("@OrderitemNo", SqlDbType.BigInt).Value = pobj.OrderitemNo;
            sqlCmd.Parameters.Add("@AmountType", SqlDbType.VarChar).Value = pobj.AmountType;
            sqlCmd.Parameters.Add("@SKU", SqlDbType.VarChar).Value = pobj.SKU;

            //-----------------------------------------------------HOLD ORDER INVOCE
            sqlCmd.Parameters.Add("@HoldDesc", SqlDbType.NVarChar).Value = pobj.HoldDesc;
            sqlCmd.Parameters.Add("@HoldTillDate", SqlDbType.NVarChar).Value = pobj.HoldTillDate;

            //-----------------------------------------------------HOLD ORDER INVOICE

            //-----------------------------------------------------REINVOICE
            sqlCmd.Parameters.Add("@ChargedAmount", SqlDbType.Decimal).Value = pobj.ChargedAmount;
            //-----------------------------------------------------REINVOICE
            sqlCmd.Parameters.Add("@ItemNo", SqlDbType.Int).Value = pobj.ItemNo;
            sqlCmd.Parameters.Add("@Pd_ItemNo", SqlDbType.Int).Value = pobj.Pd_ItemNo;

            if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
            {
                sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;

            }
            if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
            {
                sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;
            }

            if (pobj.OpCode == 61)
            {

                //-----------------------------------------------------HOLD ORDER INVOCE
                sqlCmd.Parameters.Add("@ToHoldDate", SqlDbType.DateTime).Value = pobj.ToHoldDate;
                sqlCmd.Parameters.Add("@FromHoldDate", SqlDbType.DateTime).Value = pobj.FromHoldDate;
                //-----------------------------------------------------HOLD ORDER INVOCE
            }



            sqlCmd.Parameters.Add("@TimeInterval", SqlDbType.Int).Value = pobj.TimeInterval;
            sqlCmd.Parameters.Add("@SKUType", SqlDbType.VarChar).Value = pobj.SKUType;
            if (pobj.TempHoldDate == "")
                sqlCmd.Parameters.Add("@TempHoldDate", SqlDbType.DateTime).Value = DBNull.Value;
            else
                sqlCmd.Parameters.Add("@TempHoldDate", SqlDbType.DateTime).Value = pobj.TempHoldDate;
            //dobj.SqlCmd.Parameters.Add("@CancelDate", SqlDbType.DateTime).Value = pobj.CancelDate;
            sqlCmd.Parameters.Add("@SkuID", SqlDbType.NVarChar).Value = pobj.SkuID;
            sqlCmd.Parameters.Add("@ProductId", SqlDbType.VarChar).Value = pobj.ProductId;
            if (pobj.Quantity != "" && pobj.Quantity != null)
            {
                sqlCmd.Parameters.Add("@Quantity", SqlDbType.Decimal).Value = pobj.Quantity;
            }
            else
            {
                sqlCmd.Parameters.Add("@Quantity", SqlDbType.Decimal).Value = 0;
            }
            
            sqlCmd.Parameters.Add("@OverallDiscount", SqlDbType.Decimal).Value = pobj.OverallDiscount;
            sqlCmd.Parameters.Add("@Extrashipping", SqlDbType.Decimal).Value = pobj.Extrashipping;
            sqlCmd.Parameters.Add("@FromPage", SqlDbType.NVarChar).Value = pobj.FromPage;
            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar, 25).Value = pobj.Who;
            sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.pageNo;
            sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
            sqlCmd.Parameters.Add("@onetimeonly", SqlDbType.Int).Value = pobj.OneTiemOnly;

            sqlCmd.Parameters.Add("@InvoiceProcessconti", SqlDbType.Int).Value = pobj.InvoiceProcessconti;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            pobj.ds = new DataSet();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlCmd;
            da.Fill(pobj.ds);
            if (pobj.ds != null && pobj.ds.Tables.Count != 0)
            {
                pobj.Dt = pobj.ds.Tables[0];
            }
            pobj.InvoiceNo = sqlCmd.Parameters["@InvoiceNo"].Value.ToString();
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception e)
        {
            pobj.IsException = true;
            pobj.ExceptionMessage = e.Message;
        }
    }

}