﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Net.Mail;
using System.Text;


/// <summary>
/// Summary description for BusinessUtility
/// </summary>
public class BusinessUtility
{
    public BusinessUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }
   


    public static string generatePassword()
    {
        StringBuilder randomPassword = new StringBuilder();
        Random random = new Random();
        while (randomPassword.Length <= 10)
        {
            randomPassword.Append(Convert.ToChar(random.Next(65, 122)));
        }
        return randomPassword.ToString();
    }

   public void SetDDSelectedItemByText(System.Web.UI.WebControls.DropDownList ddl, string txt)
    {
        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(txt));
    }
}




