﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessContinousOrder
/// </summary>
public class BusinessContinousOrder
{
	
    public static void insert(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 11;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void insertUsePrice(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 12;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void insertSKU(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 13;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void deleteOrderItems(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 31;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void insertSKUProducts(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 14;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void UpdateGrid(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 21;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void UpdateINV(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 22;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void updateDeclineCount(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 23;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void updateReshipStatus(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 24;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void updateReshipIntr(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 25;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void updateprocessunlock(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 32;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void updateContiInvoceStatus(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 26;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void AddContinuousOrder(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 27;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void SelectList(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 52;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void SelectInv(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 51;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void InvDetail(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 53;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void SKuDetails(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 54;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void SKuDetailforupdate(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 541;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void SearchContInv(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 55;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void bindDeclinepopup(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 56;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void shipinterval(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 57;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void countalert(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 61;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void csnotedetail(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 62;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void csnotedetail2(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 63;
        DatalayerContinousOrder.returnTable(pobj);
    }
    public static void csnotedetailbyorderno(PropertyLayerConitnousOrder pobj)
    {
        pobj.OpCode = 64;
        DatalayerContinousOrder.returnTable(pobj);
    }
}