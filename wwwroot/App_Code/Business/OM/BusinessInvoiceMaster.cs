﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessInvoiceMaster
/// </summary>
public class BusinessInvoiceMaster
{
    public static void insert(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 11;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertCont(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 12;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertTrial(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 13;
        DataInvoiceMaster.returnTable(pobj);
    }
    
    public static void inserttransactionlog(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 15;
        DataInvoiceMaster.returnTable(pobj);
    }
    
    public static void insertTempOrderItem(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 17;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertTempOrderItemProducts(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 18;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertFinalApproval(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 101;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertCont112(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 112;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void deleteTempOrderItem(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 31;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void deleteTempOrderItemandcopytdetail(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 32;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void updatecancel(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 21;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void updateinvoice(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 22;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void updateinvoiceCont(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 23;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void Refundinvoice(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 24;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdtaeContiItemMaster(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 25;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdtaeContiProductMaster(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 26;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void updateEcheckorderTohold(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 27;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdtaeContinousOrder_1(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 28;
        DataInvoiceMaster.returnTable(pobj);  
    }
    public static void UpdtaeContinousOrder_2(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 29;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void AddhappyPoint(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 210;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdateAllDetails(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 201;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdtaeContinousOrder_0(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 281;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void select(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 41;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectGrid(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 42;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectBackOrderGrid(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 421;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectInvGrid(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 43;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectBOInvGrid(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 432;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectslipDetails(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 44;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectSkuGrid(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 45;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectinvGridbyCustID(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 46;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectinvGridbySYSorder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 48;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectinvoiceNobySYSorder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 49;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectinvoicetotalItem(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 415;
        DataInvoiceMaster.returnTable(pobj);
    }
    //public static void selectUsePriceGrid(PropertyInvoiceMaster pobj)
    //{
    //    pobj.OpCode = 401;
    //    DataInvoiceMaster.returnTable(pobj);
    //}
    public static void selectUsePriceGrid2(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 4010;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectGridforRA(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 410;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void checkBackorder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 411;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void BindgenerateContinuousOrder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 413;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertintotempFromorder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 19;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void AmountPerHappyPoints(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 403;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void searchByParameters(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 61;
        DataInvoiceMaster.returnTable(pobj);
    }
   
    public static void selectGridRA(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 402;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectBarCoeNo(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 404;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectInvoiceNofromBarcode(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 405;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectAutoCompINVno(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 406;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectInvoiceItemProducts(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 407;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectShipNDisbyID(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 408;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectbackOrderPrint(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 409;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void Selectmaildetail(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 412;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectactualCharge(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 51;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void CheckTrnsID(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 700;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdateShippingDetails(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 202;
        DataInvoiceMaster.returnTable(pobj);
    }

    #region Hold Invoice & Order
    public static void updateHoldInvoice(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 203;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void updateHoldOrder(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 204;
        DataInvoiceMaster.returnTable(pobj);
    }
    #endregion
    
    #region Refund Hold/ Cancel Invoice
    public static void RefundHoldCancel(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 205;
        DataInvoiceMaster.returnTable(pobj);
    }
    #endregion
    
    #region Insert ReInvoice ChargedAmt and Process Refund
    public static void InsertReInvoiceAmt(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 102;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void UpdateInvoiceforRefund(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 206;
        DataInvoiceMaster.returnTable(pobj);
    }
    #endregion
    public static void mailconfirmation(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 207;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void BindChargeBack(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 501;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void getinvoicechargebackstatus(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 502;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void insertinvoicechargebackstatus(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 503;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void selectSendShippingMail(PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 504;
        DataInvoiceMaster.returnTable(pobj);
    }
    public static void DeleteTransactionLog (PropertyInvoiceMaster pobj)
    {
        pobj.OpCode = 212;
        DataInvoiceMaster.returnTable(pobj);
    }
}
