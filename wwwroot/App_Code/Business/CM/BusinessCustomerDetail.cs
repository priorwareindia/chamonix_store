﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessCustomerDetail
/// </summary>
public class BusinessCustomerDetail
{
    public static void selectall(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 41;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectc(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 42;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectb(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 43;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selects(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 44;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectcr(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 45;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectlogGrid(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 46;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 47;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectshippop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 48;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCrdpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 49;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCrorderdpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 50;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCrReinvoicepop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 51;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCreditCardRefundTokenpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 53;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectSaleTaxByBillingState(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 52;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCardType(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 401;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectHappyPoints(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 402;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void bindHappyPointslog(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 403;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectIDSbyCustId(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 404;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectProductSbyCustId(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 405;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void bindCustomerListPopup(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 406;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectdetailsbyBillentryID(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 407;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectdetailsbyShipentryID(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 408;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectdetailsbyCreditentryID(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 409;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void bindCustomerListPopupbyFrstName(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 410;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void GridEcheckDetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 411;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectdetailsbyEcheckID(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 412;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCLV(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 413;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectdetailsbyId(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 414;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCardNumber(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 415;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCustomerdetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 420;
        DataCustomerDetail.returnTable(pobj);
    }

    public static void selectNotesLog(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 417;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void CustomerCreditLog(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 419;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCustomerTokenById(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 421;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void GetAddressBlock(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 422;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void GetAddressBlockBulkProcess(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 423;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void checkemailid(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 4111;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void checkPhoneno(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 4112;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectpaymenttype(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 4113;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectOrderNotesLog(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 424;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void insert(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 11;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void insertwithoutCC(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 12;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void insertEcheck(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 14;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void insertCredit(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 15;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void update(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 21;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCustomer(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 22;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateBill(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 23;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateShip(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 24;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCredit(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 25;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCustpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 26;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateshippop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 27;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCrdpop(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 28;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateBillingDetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 29;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateShippingDetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 201;
        DataCustomerDetail.returnTable(pobj);
    }

    public static void updateCreditDetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 202;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateEcheckDetails(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 203;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateEcheckDefault(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 204;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCrdpop2(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 205;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCrdpop3(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 206;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCustomerToken(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 207;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateCCToken(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 208;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void checkcustphoneno(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 209;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void updateContinuousOrderDefault(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 210;
        DataCustomerDetail.returnTable(pobj);
    }
    
    public static void selectsearchby(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 61;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void searchByParameters(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 62;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void searchByParameters2(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 621;
        DataCustomerDetail.returnTable(pobj);
    }

    public static void selectTransLog(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 416;
        DataCustomerDetail.returnTable(pobj);
    }
    public static void selectCustomerType(PropertyCustomerDetail pobj)
    {
        pobj.OpCode = 418;
        DataCustomerDetail.returnTable(pobj);
    }

}