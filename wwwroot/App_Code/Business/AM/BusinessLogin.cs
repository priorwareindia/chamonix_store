﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessLogin
/// </summary>
public class BusinessLogin
{
    public static void select(PropertyLogin pobj)
    {
        pobj.OpCode = 41;
        DataLogin.returnTable(pobj);
    }

    public static void InsertLogOut(PropertyLogin pobj)
    {
        pobj.OpCode = 14;
        DataLogin.returnTable(pobj);
    }
}