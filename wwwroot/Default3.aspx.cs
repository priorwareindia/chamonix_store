﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Drawing;
public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataUtility.PageAccessHistoryLog(HttpContext.Current.Request.Url.AbsoluteUri, Session["Session_UserloginID"].ToString());
        }
        if (!IsPostBack)
        {
            try
            {
                if (Session["Session_UserloginID"] != null)
                {
                    string userType = Session["userType"].ToString();
                    if (userType != "Admin" && userType != "DevelopmentTeam")
                    {
                        Response.Redirect("AM/SignIn.aspx");

                    }
                    else
                    {
                        bindgrid();
                    }
                }
                else
                {
                    Response.Redirect("AM/SignIn.aspx");
                }
            }
            catch
            {

            }
        }
    }

    public void bindgrid()
    {

        PropertyTemporarilyImportOrders pobjcc = new PropertyTemporarilyImportOrders();
        BusinessTemporarilyImportOrders.SelectImproveBulkCarddetail(pobjcc);
        if (pobjcc.Dt.Rows.Count > 0)
        {
            ViewState["carddetail"] = pobjcc.Dt;
            gridcarddetail.DataSource = pobjcc.Dt;
            gridcarddetail.DataBind();
        }
        else
        {
            gridcarddetail.DataSource = null;
            gridcarddetail.DataBind();
        }
    }
    protected void btnprocess_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = ViewState["carddetail"] as DataTable;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            PropertyTokenization PobjToken = new PropertyTokenization();
            PobjToken.CustomerTokenId = dt.Rows[i]["TokenCustId"].ToString();
            PobjToken.CustomerCCTokenId = dt.Rows[i]["CCTokenNo"].ToString();
            PobjToken.CreditEntryId = dt.Rows[i]["CreditEntryID"].ToString();
            PobjToken.CustomerId = dt.Rows[i]["CustID"].ToString();
            PobjToken.CardNumber = dt.Rows[i]["CardNo"].ToString();
            PobjToken.ExpiryDate = dt.Rows[i]["exp"].ToString().Replace("-", "");
            PobjToken.ExpiryDate = PobjToken.ExpiryDate.Substring(4 - 2) + PobjToken.ExpiryDate.Substring(0, 2);
            PobjToken.Who = Session["Session_UserloginID"].ToString();
            BusinessTokenization.UpdateCustomerPaymentProfile(PobjToken);
            PropertyTemporarilyImportOrders pobjcc = new PropertyTemporarilyImportOrders();
            pobjcc.PageSize = Convert.ToInt32(dt.Rows[i]["AutoId"].ToString());
            pobjcc.CardNum = PobjToken.ResponseMsg;
            BusinessTemporarilyImportOrders.UpdateBulkcardImport2(pobjcc);
        }
        bindgrid();
    }
}