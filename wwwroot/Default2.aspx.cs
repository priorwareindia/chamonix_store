﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Drawing;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataUtility.PageAccessHistoryLog(HttpContext.Current.Request.Url.AbsoluteUri, Session["Session_UserloginID"].ToString());
        }
        if (!IsPostBack)
        {
            try
            {
                if (Session["Session_UserloginID"] != null)
                {
                    string userType = Session["userType"].ToString();
                    if (userType != "Admin" && userType != "DevelopmentTeam")
                    {
                        Response.Redirect("AM/SignIn.aspx");

                    }
                    else
                    {
                        bindgrid();
                    }
                }
                else
                {
                    Response.Redirect("AM/SignIn.aspx");
                }
            }
            catch
            {

            }
        }
    }
    public void bindgrid()
    {

        PropertyTemporarilyImportOrders pobjcc = new PropertyTemporarilyImportOrders();
        BusinessTemporarilyImportOrders.SelectImportCarddetail(pobjcc);
        if (pobjcc.Dt.Rows.Count > 0)
        {
            ViewState["carddetail"] = pobjcc.Dt;
            gridcarddetail.DataSource = pobjcc.Dt;
            gridcarddetail.DataBind();
        }
        else
        {
            gridcarddetail.DataSource = null;
            gridcarddetail.DataBind();
        }
    }
    protected Int32 csvImport()
    {
        //try
        //{
        DataTable dt = new DataTable();
        string line = null;
        int j = 0; int count = 0;
        using (StreamReader sr = File.OpenText(HiddenField1PhoneOrder.Value))
        {
            while ((line = sr.ReadLine()) != null)
            {
                string[] data2 = { "," };
                string[] data = line.Split(data2, StringSplitOptions.RemoveEmptyEntries);
                if (data.Length > 0)
                {
                    if (j == 0)
                    {
                        foreach (var item in data)
                        {
                            dt.Columns.Add(new DataColumn());
                        }
                        j++;
                    }
                    DataRow row = dt.NewRow();
                    if (data.Length > 0)
                    {
                        string a1 = data[0], a2 = data[1], a3 = data[2], a4 = data[3];
                        string[] arr = { a1, a3, a2, a4 };
                        row.ItemArray = arr;
                    }
                    else
                    {
                        row.ItemArray = data;
                    }
                    dt.Rows.Add(row);

                }
            }

        }
        PropertyTemporarilyImportOrders pobjcc = new PropertyTemporarilyImportOrders();
        pobjcc.ImportCreditDetail = dt;
        BusinessTemporarilyImportOrders.insertcreditdetail(pobjcc);
        return count;
        //}
        //catch (Exception ex)
        //{ return 0; }
    }
    protected void btnUplaodPhoneOrder_Click(object sender, EventArgs e)
    {
        try
        {
            int count = 0;
            PropertyTemporarilyImportOrders pobj = new PropertyTemporarilyImportOrders();
            pobj.FileName = FileUpload1PhoneOrder.FileName;
            if (FileUpload1PhoneOrder.FileName != "")
            {
                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength > 0)
                    {
                        string s = Server.MapPath("csv") + "\\" + Path.GetFileName(hpf.FileName);
                        HiddenField1PhoneOrder.Value = Server.MapPath("csv") + "\\" + Path.GetFileName(hpf.FileName);
                        hpf.SaveAs(HiddenField1PhoneOrder.Value);
                        string FileExtension = Path.GetExtension(hpf.FileName).Substring(1);
                        if (FileExtension == "csv")
                        {
                            count = csvImport();
                            ViewState["FileStatus"] = true;
                        }
                        else if (FileExtension == "txt")
                        {

                        }
                        bool fileStatus = Convert.ToBoolean(ViewState["FileStatus"]);
                        if (fileStatus == false)
                        {

                        }
                        else
                        {
                            if (count > 0)
                            {
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
        }
        catch (System.Exception ex)
        { }
    }
    protected void btnprocess_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = ViewState["carddetail"] as DataTable;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            PropertyTokenization PobjToken = new PropertyTokenization();
            PobjToken.CustomerId = dt.Rows[i]["CustID"].ToString();
            PobjToken.CardNumber = dt.Rows[i]["CardNo"].ToString();
            PobjToken.NameonCard = dt.Rows[i]["name"].ToString(); ;
            PobjToken.CardType = dt.Rows[i]["CardType"].ToString(); ;
            PobjToken.ExpiryDate = dt.Rows[i]["exp"].ToString().Replace("-", "").Replace("/", "");
            PobjToken.ExpiryDate = PobjToken.ExpiryDate.Substring(4 - 2) + PobjToken.ExpiryDate.Substring(0, 2);
            PobjToken.Who = Session["Session_UserloginID"].ToString();
            BusinessTokenization.generateCCToken(PobjToken);
            PropertyTemporarilyImportOrders pobjcc = new PropertyTemporarilyImportOrders();
            pobjcc.PageSize = Convert.ToInt32(dt.Rows[i]["AutoId"].ToString());
            pobjcc.CardNum = PobjToken.ResponseMsg;
            BusinessTemporarilyImportOrders.UpdateBulkcardImport(pobjcc);
            
        }
        bindgrid();
    }
}