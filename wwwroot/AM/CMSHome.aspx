﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/AM/CMS.master" AutoEventWireup="true" CodeFile="CMSHome.aspx.cs" Inherits="AM_CMSHome" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">


    <div class="panel panel-primary" style="align-content: center">
        <div class="panel-heading">
            <h3 class="panel-title">Customer Details</h3>
        </div>
        <div style="width: 100%">
            <center>
            <div style="width: 100%" class="floatleft">
                <table class="table table-striped table-hover " >
                    <tr>
                        <td>Customer ID</td>

                        <td>
                            <asp:Label ID="lblCustID" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                        </td>
                        <td>Alt. Customer No.</td>
                        <td>
                            <asp:Label ID="lblaltcustno" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
                 </center>


        </div>
        <div style="height: 30px; width: 100%"></div>
        <div style="width: 100%">
            <center>
                <div style="width: 50%" class="floatleft">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Billing Details
                               
                            </h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover" cellspacing="2px" style="empty-cells: hide;" align="center">
                                <tr>
                                    <td>First Name </td>
                                    <td width="200px">
                                        <asp:Label ID="lblBfstname" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                    <td>Last Name</td>

                                    <td>
                                        <asp:Label ID="lblBlastname" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Company</td>
                                    <td>
                                        <asp:Label ID="lblBcompany" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                    <td>Email ID </td>
                                    <td>
                                        <asp:Label ID="lblBemail" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td rowspan="3">Address</td>
                                    <td rowspan="3">
                                        <asp:TextBox ID="lblBaddress1" runat="server" Enabled="False" ForeColor="#336699" Height="65" Width="150px"
                                            Style="background-color: #FFFFFF;" CssClass="formTextBoxes" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td>Home Phone</td>
                                    <td>
                                        <asp:Label ID="lblBHphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>


                                <tr>

                                    <td>Mobile</td>
                                    <td>
                                        <asp:Label ID="lblBphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Office Phone</td>
                                    <td>
                                        <asp:Label ID="lblBofphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <div style="width: 50%" class="floatright">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Shipping Details 
                               
                            </h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover" cellspacing="2px" style="empty-cells: hide;">

                                <tr>
                                    <td>First Name </td>
                                    <td width="200px">
                                        <asp:Label ID="lblShfstname" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                    <td>Last Name</td>

                                    <td>
                                        <asp:Label ID="lblShlastname" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Company</td>
                                    <td>
                                        <asp:Label ID="lblShcompany" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                    <td>Email ID </td>
                                    <td>
                                        <asp:Label ID="lblemail" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3">Address</td>
                                    <td rowspan="3">
                                        <asp:TextBox ID="lblShaddress1" runat="server" ReadOnly="true" ForeColor="#336699" Height="65" Width="150px" Style="background-color: #FFFFFF;" CssClass="formTextBoxes" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td>Home Phone</td>
                                    <td>
                                        <asp:Label ID="lblShphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>

                                <tr>

                                    <td>Mobile</td>
                                    <td>
                                        <asp:Label ID="lblShHphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Office Phone</td>
                                    <td>
                                        <asp:Label ID="lblShofphone" runat="server" Enabled="False" ForeColor="#336699" Width="150" Height="20" CssClass="formTextBoxes"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

             

            </center>
        </div>
    </div>


    <asp:TabContainer ID="TChistory" runat="server" Width="99%" Style="text-align: center;" ActiveTabIndex="3" CssClass="NewsTab">


        <asp:TabPanel ID="TbInvoiceLog" runat="server" HeaderText="Invoice List" CssClass="tabPage">
            <ContentTemplate>

                <div style="border: 1px solid #0099FF; overflow: auto;">
                    <center>
                        <asp:GridView ID="GridInvoice" runat="server"  AutoGenerateColumns="False"
                            Width="98%" AllowSorting="True" OnSorting="GridInvoice_Sorting"  >
                            <Columns>
                                <asp:TemplateField HeaderText="SYS Order No" SortExpression="SYSOrderNo" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsysorderno" runat="server" Text='<%#Eval("SYSOrderNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" BorderStyle="Outset" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Invoice No" SortExpression="InvoiceNo" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lnkInvoiceNo" runat="server"  Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" BorderStyle="Outset" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="InvoiceType" HeaderText="Invoice Type" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" SortExpression="InvoiceDate"
                                    HeaderStyle-ForeColor="#F1F78E" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="InvoiceStatus" runat="server" Text='<% #Eval("InvoiceStatus")%>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" BorderStyle="Outset" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TrackingUrl" HeaderStyle-CssClass="displayNone" ItemStyle-CssClass="displayNone" FooterStyle-CssClass="displayNone">
                                    <ItemTemplate>
                                        <asp:Label ID="TrackingUrl" runat="server" Text='<% #Eval("TrackingUrl")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BillingAddress" HeaderText="Bill To Address" />
                                <asp:BoundField DataField="ShippingAddress" HeaderText="Ship To Address" />
                                <asp:BoundField DataField="BillTotalamt" HeaderText="Total Amount" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right">
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Tracking No." ItemStyle-Width="90px" HeaderStyle-Width="90px"
                                    HeaderStyle-ForeColor="#F1F78E" ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="TrackingNo" OnClick="btnTrackingNo_Click" runat="server"
                                            Text='<% #Eval("TrackingNo")%>'></asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:BoundField DataField="SaleSource" HeaderText="Sale Source" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                               

                                
     
                            </Columns>
                            <HeaderStyle CssClass="GridviewScrollHeader" />
                            <RowStyle CssClass="GridviewScrollItem" />
                        </asp:GridView>
                    </center>
                </div>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel ID="TbReshipList" runat="server" HeaderText="Club Membership List">
            <ContentTemplate>
                <div style="border: 1px solid #0099FF; overflow: auto;">
                    <center>
                        <asp:GridView ID="GridReShipList" runat="server" CellPadding="4" GridLines="Both" AutoGenerateColumns="False"
                            Width="98%" AllowSorting="True" OnSorting="GridReShipList_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="Order Number" SortExpression="SYSorderno" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SYSorderno") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer ID" SortExpression="CustID" Visible="false" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreCustID" runat="server" Text='<%# Bind("CustID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer Name" SortExpression="Custname" Visible="false" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreCustomername" runat="server" Text='<%# Bind("Custname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Signup Invoice" SortExpression="InvoiceNo" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreInvoiceNo" runat="server" Text='<%# Bind("InvoiceNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Signup Date" SortExpression="Orderdate" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="Label25" runat="server" Text='<%# Bind("Orderdate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Signup SKU" SortExpression="SKUID" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreSKUID" runat="server" Text='<%# Bind("SKUID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Current Product" DataField="Productname" HtmlEncode="false" />

                                <asp:TemplateField HeaderText="Next Invoice Amt" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblretotalamt" runat="server" Text='<%# Bind("ReshipAmt") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Source Key">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSourceKey" runat="server" Text='<%# Bind("SourceKey") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Count" HeaderText="Shipment Count" ItemStyle-Width="2%" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="lastinvoice" HeaderText="Last Invoice" SortExpression="lastinvoice" HeaderStyle-ForeColor="#F1F78E" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />

                                <asp:TemplateField HeaderText="Last Invoice Date" SortExpression="PrevShipdate" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrePrevreShipDate" runat="server" Text='<%# Bind("PrevShipdate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Next Invoice Date" SortExpression="NextShipDate" HeaderStyle-ForeColor="#F1F78E">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreNextreShipDate" runat="server" Text='<%# Bind("NextShipDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtNxtShipD" Text='<%# Bind("NextShipDate") %>' runat="server" ></asp:TextBox>
                                        <asp:CalendarExtender ID="CCtxtNxtShipD" runat="server" DefaultView="Days" Format="MM/dd/yyyy"
                                            TargetControlID="txtNxtShipD">
                                        </asp:CalendarExtender>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Interval">
                                    <ItemTemplate>
                                        <asp:Label ID="LblreShipIntrvl" runat="server" Text='<%# Bind("ShippingInterval") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtreShipIntrvl" runat="server" Text='<%# Bind("ShippingInterval") %>'
                                            Width="68px"  ></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" TargetControlID="txtreShipIntrvl"
                                            FilterType="Numbers" runat="server">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                               

                            </Columns>
                            <HeaderStyle CssClass="GridviewScrollHeader" />
                            <RowStyle CssClass="GridviewScrollItem" />
                        </asp:GridView>
                    </center>
                </div>
            </ContentTemplate>
        </asp:TabPanel>

          <asp:TabPanel ID="tbProductLog" runat="server" HeaderText="Ordered Products List">
            <ContentTemplate>
                <div style="border: 1px solid #0099FF; overflow: auto;">
                    <center>
                        <asp:GridView ID="GridProductLog" runat="server" AutoGenerateColumns="false" AllowSorting="True"
                            OnSorting="GridProductLog_Sorting" Width="98%" OnRowDataBound="GridProductLog_RowDataBound">
                            <Columns>

                                <asp:TemplateField HeaderText="Invoice No" SortExpression="SYSOrderNo" HeaderStyle-ForeColor="#F1F78E" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:Label ID="lnkProInvoiceNo" runat="server"
                                            Text='<%#Eval("InvoiceNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" BorderStyle="Outset" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SkuID" SortExpression="SkuID" HeaderStyle-ForeColor="#F1F78E" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:Label ID="lnkProSkuID" runat="server" Text='<%#Eval("SkuID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" BorderStyle="Outset" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:BoundField DataField="Quantity" HeaderText="Sku Quantity" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px" />
                                <asp:BoundField DataField="SKUDescrip" HeaderText="Description" ItemStyle-Width="100px" />
                                <asp:BoundField DataField="ProductName" HeaderText="Product" ItemStyle-Width="50px" />
                                <asp:BoundField DataField="spquantity" HeaderText="Product Quantity" ItemStyle-Width="25px" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" ItemStyle-Width="50px"
                                    SortExpression="InvoiceDate" HeaderStyle-ForeColor="#F1F78E" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="OrderProcessingDate" HeaderText="Invoice Processing Date"
                                    SortExpression="OrderProcessingDate" HeaderStyle-ForeColor="#F1F78E" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Product_Price" HeaderText="Product Price" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Sku_price" HeaderText="SKU Price"
                                    SortExpression="SKU Price" HeaderStyle-ForeColor="#F1F78E" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <HeaderStyle CssClass="GridviewScrollHeader" />
                            <RowStyle CssClass="GridviewScrollItem" />
                        </asp:GridView>
                    </center>
                </div>
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>




</asp:Content>

