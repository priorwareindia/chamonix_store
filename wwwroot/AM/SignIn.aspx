﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Inherits="AM_SignIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Unimed Login</title>

    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
    <link href="../ScriptsAndCSS/CSS/login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function toggle_visibility(id) {
            var e = document.getElementById(id);

            var l = document.getElementById('Maindiv');
            if (e.style.display == 'block') {
                e.style.display = 'none';
                l.style.display = 'block';
            }
            else {
                e.style.display = 'block';
                l.style.display = 'none';
            }
        }
    </script>

</head>
<body class="login-page" >
    <form id="f" runat="server" >
        <div id="logo">
            <!--main div-->
        </div>
        <!--end main div-->
        <center>
            <div class="login-wrapper">
                <div class="loginHeader">
                    <center>
                        <div class="logo-wrap">
                            <img src="../ScriptsAndCSS/CSS/Images/chamonix-logo.svg"></img>
                          
                        </div>
                    </center>
                </div>

                <center>
                    <div class="LoginBody">
                        <h3>Detail of Customer Order</h3>
                        <div class="content">
                            <asp:Panel ID="Maindiv" runat="server">
                                <div class="form-group">

                                    <div class="controls">
                                        <asp:TextBox ID="txtmail" runat="server" placeholder="Email ID" autocomplete="off" type="text" class="form-control"></asp:TextBox>
                                       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls">
                                        <asp:TextBox ID="txtzip" class="form-control" placeholder="Zipcode" runat="server"
                                            type="text"></asp:TextBox>
                                    </div>
                                </div>

                               
                                <div class="form-group">
                                    <div class="controls">
                                        <asp:Button ID="btnlogin" runat="server" Text="Proceed" class="btn btn-primary form-control" CausesValidation="true"
                                             onclick="btnlogin_Click"  />
                                    </div>                                 
                                  
                                </div>
                                <div class="form-group">
                                    <div class="controls">
                                        <asp:Label ID="lblmessage" CssClass="error" runat="server"
                                            Text="Unable to proceed please fill correct Email ID and Zipcode!!!"></asp:Label>
                                    </div>
                                </div>
                            </asp:Panel>
                            <p class="clearfix">
                            </p>
                            <p class="date">
                                <asp:Label ID="lblDate" runat="server" Text="Label" Visible="False"></asp:Label>
                            </p>
                           <%-- <div id="Forgot2" visible="false" runat="server">--%>
                                <asp:Panel ID="Forgot" runat="server" Visible="false">
                                    <div class="form-group">
                                        <div class="controls">
                                            <%--<a onclick="toggle_visibility('Forgot2')" id="A1">Suddenly Remmebered ! </a>--%>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:TextBox ID="txtfmail" placeholder="Login ID" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                             
                                    <asp:Label ID="lblcheq" runat="server" Font-Bold="False" Font-Size="Large" ForeColor="Red"
                                        Font-Names="Comic Sans MS"></asp:Label>
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:Label ID="lblforgetmessage" CssClass="error" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </asp:Panel>
                           <%-- </div>--%>
                        </div>
                        <center>
                            
                            
                            <p class="copyright" style="width: 50%">
                                Copyright &copy; 2020. All Rights Reserved.
                            </p>
                        </center>
                    </div>
                </center>
            </div>
        </center>
    </form>
</body>
</html>
