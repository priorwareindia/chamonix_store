﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Xml;
using Newtonsoft.Json.Linq;

public partial class AM_SignIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            //MailSending.SendInvoice("INV10808045");
            //PropertyTokenization pobj = new PropertyTokenization();
            //BusinessTokenization.GetTransactionDetails(pobj);

            DataUtility dobj = new DataUtility();
            if (!IsPostBack)
            {
                lblDate.Text = DateTime.Today.ToShortDateString();
                lblmessage.Visible = false;
                lblforgetmessage.Visible = false;
                ViewState.Add("CheckForget", "");

                Session.Abandon(); // Does nothing
                Session.Clear();
            }
            else
            {
                string a = ViewState["CheckForget"].ToString();
                if (ViewState["CheckForget"].ToString() == "ForgetPassword")
                {
                    Maindiv.Visible = false;
                    Forgot.Visible = true;
                }
                else
                {
                    Maindiv.Visible = true;
                    Forgot.Visible = false;
                }
            }
        }
        catch { }
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {

        try
        {
            string userid;
            PropertyLogin pobj = new PropertyLogin();
            DataUtility dobj = new DataUtility();
            pobj.EmailID = txtmail.Text.Trim();
            pobj.Zipcode = txtzip.Text.Trim();
            pobj.IpAddress = HttpContext.Current.Request.UserHostAddress;
            Maindiv.Visible = true;
            Forgot.Visible = false;



            BusinessLogin.select(pobj);
            if (!pobj.IsException && pobj.Dt.Rows.Count == 1)
            {
                userid = pobj.Dt.Rows[0]["CustID"].ToString();
              

                if (userid != "")
                {
                    Session.Add("Session_UserloginID", userid);
         
                    Response.Redirect("~/AM/CMSHome.aspx",false);

                }
                else
                {
                    lblmessage.Text = "Unable to proceed please fill correct Email ID and Zipcode!!!";
                    lblmessage.Visible = true;
                }

            }
            else
            {
                lblmessage.Text = "Unable to proceed please fill correct Email ID and Zipcode!!!";
                lblmessage.Visible = true;
            }

        }
        catch (Exception ex)
        { }



    }
  
}