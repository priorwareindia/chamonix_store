﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class AM_CMS : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["Session_UserloginID"] == null)
        {
            Response.Redirect("~/AM/SignIn.aspx");
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));

        PropertyLogin pobj = new PropertyLogin();
        if (!IsPostBack)
        {
            if (Session["Session_UserloginID"] != null)
            {
                DataUtility dobj = new DataUtility();
                string home = "";
                home = Convert.ToString(dobj.stringFuntion(home));
                lblhome.Text = home;
                string userType = "Admin";
                if (userType != "Admin" && userType != "DevelopmentTeam")
                {
                    userAccessModuleCheckList();
                    userAccessSubModuleCheckList();
                }

            }
            else
            {
                MPLogin.Show();
                //Response.Redirect("~/AM/SignIn.aspx");
            }
 
     
            #region Menu & Sub Menu CSS
            string pageUrl = Request.Url.AbsolutePath.ToString();
            pageUrl = pageUrl.Replace("/", "");
            clearMenuCSS();
            switch (pageUrl.ToString())
            {
                case "AMCMSHome.aspx": lbtnhome.CssClass = "current"; break;
              
            }
            #endregion
           

        }
        POPUP1.Style.Add("display", "show");
    }

    protected void MPEMessageStyle1(int styleCode, string msg)
    {
        try
        {
            lblMPEMessage1.Text = "";
            switch (styleCode)
            {
                case 1:
                    pnlMPEMessageHeader1.CssClass = "popup_TitlebarWarning";
                    MPEMessagePNL1.CssClass = "popupBodyDivWarning";
                    btnMPEClose1.CssClass = "btn btn-warning btn-sm";
                    btnMPEClose1.Text = "Close";
                    lblMPEMessage1.Text = msg;
                    break;
                case 2:
                    pnlMPEMessageHeader1.CssClass = "popup_TitlebarError";
                    MPEMessagePNL1.CssClass = "popupBodyDivError";

                    btnMPEClose1.CssClass = "btn btn-danger btn-sm";
                    lblMPEMessage1.Text = msg;
                    break;
                case 3:
                    pnlMPEMessageHeader1.CssClass = "popup_TitlebarSuccess";
                    MPEMessagePNL1.CssClass = "popupBodyDivSuccess";

                    lblMPEMessage1.Text = msg;
                    break;
                case 4:
                    pnlMPEMessageHeader1.CssClass = "popup_TitlebarInformation";
                    MPEMessagePNL1.CssClass = "popupBodyDivInformation";

                    btnMPEClose1.CssClass = "btn btn-primary btn-sm";
                    lblMPEMessage1.Text = msg;
                    break;
            }
            MPEMessage1.Show();
        }
        catch (Exception ex)
        {
            //MPEMessageStyle(1, "some thing wrong please contact the administration");
        }
    }
    private void userAccessModuleCheckList()
    {
        System.Data.DataTable dtUserAccessModule = new System.Data.DataTable();
        dtUserAccessModule = Session["UserAccessModule"] as System.Data.DataTable;
    
    }
    private void userAccessModule(System.Data.DataTable dtUserAccessModule, LinkButton lbtn)
    {
        lbtn.Visible = false;
        foreach (System.Data.DataRow dr in dtUserAccessModule.Rows)
        {
            if (lbtn.Text == dr["ModuleName"].ToString())
            {
                lbtn.Visible = true;
                break;
            }
        }
    }
    private void userAccessSubModuleCheckList()
    {
        System.Data.DataTable dtUserAccessSubModule = new System.Data.DataTable();
        dtUserAccessSubModule = Session["UserAccessSubModule"] as System.Data.DataTable;
        #region
        userAccessSubModule(dtUserAccessSubModule, lnkCustMerge);
        userAccessSubModule(dtUserAccessSubModule, lnkCustList);
        userAccessSubModule(dtUserAccessSubModule, lnkcustcard);
        userAccessSubModule(dtUserAccessSubModule, lnkPaymentProfileDetail);

        userAccessSubModule(dtUserAccessSubModule, lnkCategory);
        userAccessSubModule(dtUserAccessSubModule, lnkRawMaterial);
        userAccessSubModule(dtUserAccessSubModule, lnkProduct);
        userAccessSubModule(dtUserAccessSubModule, lnkProductStock);
        userAccessSubModule(dtUserAccessSubModule, lnkSKUmgmt);
        userAccessSubModule(dtUserAccessSubModule, lnkSupplier);
        userAccessSubModule(dtUserAccessSubModule, lnkProductOrder);
        userAccessSubModule(dtUserAccessSubModule, lnkPurchaseOrder);

        userAccessSubModule(dtUserAccessSubModule, lnkManualOrder);
        userAccessSubModule(dtUserAccessSubModule, lnkHoldOrder);
        userAccessSubModule(dtUserAccessSubModule, lnkorderProc);
        userAccessSubModule(dtUserAccessSubModule, lnkapiorderProc);
        userAccessSubModule(dtUserAccessSubModule, lnkinvoiceProc);
        userAccessSubModule(dtUserAccessSubModule, lnkInvoiceList);
        userAccessSubModule(dtUserAccessSubModule, lnkReshipList);
        userAccessSubModule(dtUserAccessSubModule, lnkRechargeList);
        userAccessSubModule(dtUserAccessSubModule, lnkBatchLog);
        userAccessSubModule(dtUserAccessSubModule, lnkInvoiceBatchLog);
        userAccessSubModule(dtUserAccessSubModule, lnkCCExpiry);

        //userAccessSubModule(dtUserAccessSubModule, lnkEndiciaAcc);
        //userAccessSubModule(dtUserAccessSubModule, lnkGetLabel);
        //userAccessSubModule(dtUserAccessSubModule, lnkEndiciaLog);
        //userAccessSubModule(dtUserAccessSubModule, lnkShippingLog);

        userAccessSubModule(dtUserAccessSubModule, lnkRMAList);
        userAccessSubModule(dtUserAccessSubModule, lnkRefundList);
        userAccessSubModule(dtUserAccessSubModule, lnkRADiscount);
        userAccessSubModule(dtUserAccessSubModule, lnkInstantRef);
        userAccessSubModule(dtUserAccessSubModule, lnkInstantRefReq);

        userAccessSubModule(dtUserAccessSubModule, lnkParameterSettings);
        userAccessSubModule(dtUserAccessSubModule, lnkAPIParameterSettings);
        userAccessSubModule(dtUserAccessSubModule, lnkShippingMaster);
        userAccessSubModule(dtUserAccessSubModule, lnkEmailReceiver);
        userAccessSubModule(dtUserAccessSubModule, lnkEmailServer);
        userAccessSubModule(dtUserAccessSubModule, lnkSaletax);
        userAccessSubModule(dtUserAccessSubModule, lnkCardType);
        userAccessSubModule(dtUserAccessSubModule, lnkEmailSett);
        userAccessSubModule(dtUserAccessSubModule, lnkCompanyProfile);
        userAccessSubModule(dtUserAccessSubModule, lnkSupportEmailPriority);
        userAccessSubModule(dtUserAccessSubModule, lnkSourceKeyAssign);
        userAccessSubModule(dtUserAccessSubModule, lnknoreplyemail); 
            userAccessSubModule(dtUserAccessSubModule, lnkunimedApiDetail);
        userAccessSubModule(dtUserAccessSubModule, lnkAuthenticPermission);


        //userAccessSubModule(dtUserAccessSubModule, lnkSalesSummary);
        //userAccessSubModule(dtUserAccessSubModule, lnkDailysale);
        //userAccessSubModule(dtUserAccessSubModule, lnkMediaTracking);
        //userAccessSubModule(dtUserAccessSubModule, lnkMediaWiseOrder);
        //userAccessSubModule(dtUserAccessSubModule, lnkDateWiseInvoice);
        userAccessSubModule(dtUserAccessSubModule, lnkSourceKeySale);
        userAccessSubModule(dtUserAccessSubModule, lblMarketingReport);
        userAccessSubModule(dtUserAccessSubModule, lblMarketingReportB);
        userAccessSubModule(dtUserAccessSubModule, lblMarketingReportC);
        userAccessSubModule(dtUserAccessSubModule, lblMediaReportA);
        userAccessSubModule(dtUserAccessSubModule, lblMediaReportB);
        userAccessSubModule(dtUserAccessSubModule, lblProductSaleReport);
        //userAccessSubModule(dtUserAccessSubModule, lblUnsettledTransaction);
        userAccessSubModule(dtUserAccessSubModule, lblMonthtoMonthReport);

        userAccessSubModule(dtUserAccessSubModule, lblAutoshipReport);
        userAccessSubModule(dtUserAccessSubModule, lblOrderSourceReport);


        userAccessSubModule(dtUserAccessSubModule, lnkHearAboutUs);
        userAccessSubModule(dtUserAccessSubModule, lnkOfferMaster);
        userAccessSubModule(dtUserAccessSubModule, Lnk800Master);
        userAccessSubModule(dtUserAccessSubModule, lnkProgramType);
        userAccessSubModule(dtUserAccessSubModule, lnkMediaCost);
        userAccessSubModule(dtUserAccessSubModule, lnkSpotType);
        userAccessSubModule(dtUserAccessSubModule, lnkScript);
        userAccessSubModule(dtUserAccessSubModule, LnkProductAdvertised);
        userAccessSubModule(dtUserAccessSubModule, LnkSourceKey);
        userAccessSubModule(dtUserAccessSubModule, lnkCallcentre);
        userAccessSubModule(dtUserAccessSubModule, lnkCopyMaster);
        userAccessSubModule(dtUserAccessSubModule, LnkMedaiCategory);

        userAccessSubModule(dtUserAccessSubModule, lnkCSMCustomerList);
        userAccessSubModule(dtUserAccessSubModule, lnkSupportEmailList);
        userAccessSubModule(dtUserAccessSubModule, lnkUserEmailList);
        userAccessSubModule(dtUserAccessSubModule, lnkComposeEmail);
        userAccessSubModule(dtUserAccessSubModule, lnkComposedEmaillist);
        userAccessSubModule(dtUserAccessSubModule, lnkBlockAddress);

        userAccessSubModule(dtUserAccessSubModule, lnkContract);
        userAccessSubModule(dtUserAccessSubModule, lnkContractList);
        userAccessSubModule(dtUserAccessSubModule, lnkCampaignMaster);
        userAccessSubModule(dtUserAccessSubModule, lnkMediaAccounting);

        userAccessSubModule(dtUserAccessSubModule, lnkuserlist);
        userAccessSubModule(dtUserAccessSubModule, lnkadduser);
        userAccessSubModule(dtUserAccessSubModule, lnkemployeeSettings);
        userAccessSubModule(dtUserAccessSubModule, lnkusertypemaster);

        userAccessSubModule(dtUserAccessSubModule, lnkErrTForm);
        userAccessSubModule(dtUserAccessSubModule, lnkErrTLog);
        userAccessSubModule(dtUserAccessSubModule, lnkRefundableTaxTLog);
        userAccessSubModule(dtUserAccessSubModule, lnkChargeableTaxTLog);
        #endregion
    }
    private void userAccessSubModule(System.Data.DataTable dtUserAccessSubModule, LinkButton lbtn)
    {
        lbtn.Visible = false;
        foreach (System.Data.DataRow dr in dtUserAccessSubModule.Rows)
        {
            if (lbtn.Text == dr["subModuleName"].ToString())
            {
                lbtn.Visible = true;
                break;
            }
        }
    }
    private void clearMenuCSS()
    {
    }
    private void cleareSubmenuCss()
    {
        lnkCustMerge.CssClass = null;
        lnkCustList.CssClass = null;
        lnkcustcard.CssClass = null;
        lnkPaymentProfileDetail.CssClass = null;

        lnkCategory.CssClass = null;
        lnkRawMaterial.CssClass = null;
        lnkProduct.CssClass = null;
        lnkProductStock.CssClass = null;
        lnkSKUmgmt.CssClass = null;
        lnkSupplier.CssClass = null;
        lnkProductOrder.CssClass = null;
        lnkPurchaseOrder.CssClass = null;

        lnkManualOrder.CssClass = null;
        lnkHoldOrder.CssClass = null;
        lnkorderProc.CssClass = null;
        lnkapiorderProc.CssClass = null;
        lnkinvoiceProc.CssClass = null;
        lnkInvoiceList.CssClass = null;
        lnkReshipList.CssClass = null;
        lnkRechargeList.CssClass = null;
        lnkBatchLog.CssClass = null;
        lnkInvoiceBatchLog.CssClass = null;
        lnkCCExpiry.CssClass = null;

        //lnkEndiciaAcc.CssClass = null;
        //lnkGetLabel.CssClass = null;
        //lnkEndiciaLog.CssClass = null;
        //lnkShippingLog.CssClass = null;

        lnkRMAList.CssClass = null;
        lnkRefundList.CssClass = null;
        lnkRADiscount.CssClass = null;
        lnkInstantRef.CssClass = null;
        lnkInstantRefReq.CssClass = null;

        lnkParameterSettings.CssClass = null;
        lnkAPIParameterSettings.CssClass = null;
        lnkShippingMaster.CssClass = null;
        lnkEmailReceiver.CssClass = null;
        lnkEmailServer.CssClass = null;
        lnkSaletax.CssClass = null;
        lnkCardType.CssClass = null;
        lnkEmailSett.CssClass = null;
        lnkCompanyProfile.CssClass = null;
        lnkSupportEmailPriority.CssClass = null;
        lnkSourceKeyAssign.CssClass = null;
        lnknoreplyemail.CssClass = null;
        lnkunimedApiDetail.CssClass = null;
        lnkAuthenticPermission.CssClass = null;
        lnkHearAboutUs.CssClass = null;
        lnkOfferMaster.CssClass = null;

        //lnkSalesSummary.CssClass = null;
        //lnkDailysale.CssClass = null;
        //lnkMediaTracking.CssClass = null;
        //lnkMediaWiseOrder.CssClass = null;
        //lnkDateWiseInvoice.CssClass = null;
        lnkSourceKeySale.CssClass = null;
        lblMarketingReport.CssClass = null;
        lblMarketingReportB.CssClass = null;
        lblMarketingReportC.CssClass = null;
        lblMediaReportA.CssClass = null;
        lblMediaReportB.CssClass = null;
        lblOrderSourceReport.CssClass = null;
        lblAutoshipReport.CssClass = null;
        lblMonthtoMonthReport.CssClass = null;
        lblProductSaleReport.CssClass = null;
        //lblUnsettledTransaction.CssClass = null;

        Lnk800Master.CssClass = null;
        lnkProgramType.CssClass = null;
        lnkMediaCost.CssClass = null;
        lnkSpotType.CssClass = null;
        lnkScript.CssClass = null;
        LnkProductAdvertised.CssClass = null;
        LnkSourceKey.CssClass = null;
        lnkCallcentre.CssClass = null;
        lnkCopyMaster.CssClass = null;
        LnkMedaiCategory.CssClass = null;

        lnkContract.CssClass = null;
        lnkContractList.CssClass = null;
        lnkCampaignMaster.CssClass = null;
        lnkMediaAccounting.CssClass = null;

        lnkuserlist.CssClass = null;
        lnkadduser.CssClass = null;
        lnkemployeeSettings.CssClass = null;
        lnkusertypemaster.CssClass = null;

        lnkErrTForm.CssClass = null;
        lnkErrTLog.CssClass = null;
        lnkRefundableTaxTLog.CssClass = null;
        lnkChargeableTaxTLog.CssClass = null;

        lnkCustList.CssClass = null;
        lnkCustMerge.CssClass = null;
    }
    protected void change_OnClick(object sender, EventArgs e)
    {
        LinkButton lbtn = sender as LinkButton;

        switch (lbtn.Text)
        {
            case "Home": lbtnhome.CssClass = "current"; break;
        }

    }
    private void setCurrentMainMenuSession(LinkButton lbtn, int mainMenuCode)
    {

        Session.Add("ActiveView", MVSidebarMenu.ActiveViewIndex = mainMenuCode);
        Session.Add("lbtn", lbtn);
        if (mainMenuCode == 0)
        {
            Response.Redirect("~/AM/CMSHome.aspx");
        }
        else
            Response.Redirect("~/AM/CMSHome.aspx");
    }
    private void setCurrentSubMenuSession(LinkButton lbtn, string redirectPath)
    {
        hdn_mainmenu_index.Value = lbtn.Text;
        Response.Redirect(redirectPath);
    }
    protected void lbtnlog_OnClick(object sender, EventArgs e)
    {
        PropertyLogin pobj = new PropertyLogin();
        //pobj.Userid = Session["Session_UserloginID"].ToString();
        //pobj.IpAddress = HttpContext.Current.Request.UserHostAddress;
        BusinessLogin.InsertLogOut(pobj);
        Session.Abandon(); // Does nothing
        Session.Clear();
        Response.Redirect("~/AM/SignIn.aspx");
    }
    protected void LBtnProductStock_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/IM/ProductMaster.aspx");
    }
    protected void LBtnREship_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/OM/ReShipList.aspx");
    }


    protected void btnlogin_Click(object sender, EventArgs e)
    {
        try
        {
            string userid;
            PropertyLogin pobj = new PropertyLogin();
            DataUtility dobj = new DataUtility();
            pobj.EmailID = txtmail.Text.Trim();
            pobj.Zipcode = txtzip.Text.Trim();
            BusinessLogin.select(pobj);
            if (!pobj.IsException && pobj.Dt.Rows.Count == 1)
            {
                userid = pobj.Dt.Rows[0]["CustID"].ToString();
                Session.Add("Session_UserloginID", userid);
                if (userid != "")
                {
                    Response.Redirect("~/AM/CMSHome.aspx");

                }

            
            MPLogin.Hide();
                //Response.Redirect("~/AM/CMSHome.aspx");
            }
            else
            {
                lblmessage.Text = "UserID or Password may be incorrect or your account has been deactivated!!!";
                lblmessage.Visible = true;
                MPLogin.Show();
            }
        }
        catch (Exception ex)
        { }
    }
}
