﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Web.Services;
public partial class AM_CMSHome : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {
        bindInvoiceGrid();
        bindform();
        bindgridReship();
        bindOrderProductGrid();


    }

    private void bindform()
    {
        PropertyCustomerDetail pobj = new PropertyCustomerDetail();
        if (Session["Session_UserloginID"] != null)
        {
            pobj.CustID = Session["Session_UserloginID"].ToString();
            lblCustID.Text = Convert.ToString(pobj.CustID);
            BusinessCustomerDetail.selectdetailsbyId(pobj);
            {
                if (pobj.Dt.Rows.Count > 0)
                {
                    #region Credit calculation
                    string Creditgvn = pobj.Dt.Rows[0]["CreditAmt"].ToString().Trim();
                    string CreditUsed = pobj.Dt.Rows[0]["CreditUsed"].ToString().Trim();
                    
                    #endregion

                    #region Customerdeatials
                    BusinessCustomerDetail.selectCustomerdetails(pobj);
                    if (!pobj.IsException)
                    {
                        if (pobj.ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow ds in pobj.ds.Tables[0].Rows)
                            {
                                lblaltcustno.Text = ds["AlterNumber"].ToString();
                                

                            }
                        }
                        if (pobj.ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow ds in pobj.ds.Tables[1].Rows)
                            {
                                lblBfstname.Text = ds["FirstName"].ToString();
                                lblBlastname.Text = ds["LastName"].ToString();

                                lblBcompany.Text = ds["Company"].ToString();
                                lblBemail.Text = ds["EmailID"].ToString();

                                lblBaddress1.Text = ds["Address"].ToString();

                                lblBHphone.Text = ds["PhoneNo"].ToString();
                                lblBphone.Text = ds["PhoneNo1"].ToString();
                                lblBofphone.Text = ds["OfficeNoB"].ToString();
                            }
                        }

                        if (pobj.ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow ds in pobj.ds.Tables[2].Rows)
                            {
                                lblShfstname.Text = ds["STFirstName"].ToString();
                                lblShlastname.Text = ds["STLastName"].ToString();

                                lblShcompany.Text = ds["STCompany"].ToString();
                                lblemail.Text = ds["STEmailID"].ToString();

                                lblShaddress1.Text = ds["Address"].ToString();

                                lblShphone.Text = ds["STPhoneNo"].ToString();
                                lblShHphone.Text = ds["STPhoneNo1"].ToString();
                                lblShofphone.Text = ds["SofficeNo"].ToString();
                            }
                        }

                    }
                    #endregion

                    pobj.CustID = lblCustID.Text.Trim();
                   
           
                }
            }
        }
    }



    #region Invoice List
    protected void GridInvoice_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            //GridInvoice.DataSource = sortedView;
            //GridInvoice.DataBind();

        }
        catch { }
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected DataTable sortbindgrid()
    {
        DataTable dt = ViewState["dtinvoce"] as DataTable;
        return dt;
    }
    private void bindInvoiceGrid()
    {
        try
        {
            PropertyInvoiceMaster pobj = new PropertyInvoiceMaster();
            {
                pobj.CustID = Session["Session_UserloginID"].ToString();
            }
            BusinessInvoiceMaster.selectinvGridbyCustID(pobj);
            if (Session["Session_UserloginID"].ToString() != "")
                ViewState["dtinvoce"] = pobj.Dt;
                    GridInvoice.DataSource = pobj.Dt;
                    GridInvoice.DataBind();
        }
        catch { }
    }
    protected void btnTrackingNo_Click(object sender, EventArgs e)
    {
        try
        {
            PropertyInvoiceMaster pobj = new PropertyInvoiceMaster();
            foreach (GridViewRow grs in GridInvoice.Rows)
            { grs.Cells[0].CssClass = "GridviewScrollItem"; }
            GridViewRow gr2 = ((sender as LinkButton).Parent.Parent) as GridViewRow;
            gr2.Cells[0].CssClass = "selected";
            string TrackingUrl = ((Label)gr2.FindControl("TrackingUrl")).Text;
            string Trackingno = (sender as LinkButton).Text;
            string url = TrackingUrl + Trackingno;
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "window.open('" + url + "');", true); ;
        }
        catch (Exception ex)
        {

        }
    }
    #endregion


    #region Clubmembership List
    protected void GridReShipList_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid8());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            GridReShipList.DataSource = sortedView;
            GridReShipList.DataBind();

        }
        catch { }
    }
    protected DataTable sortbindgrid8()
    {

        DataTable dt = ViewState["Reshipdt"] as DataTable;
        return dt;
    }
    private void bindgridReship()
    {
        try
        {
            PropertyLayerConitnousOrder pobjCo = new PropertyLayerConitnousOrder();
            pobjCo.ToNxtShpdate = Convert.ToDateTime("01/01/2010");
            pobjCo.FromNxtShpdate = Convert.ToDateTime("01/01/2010");
            pobjCo.FromLstShpdate = Convert.ToDateTime("01/01/2010");
            pobjCo.ToLstShpdate = Convert.ToDateTime("01/01/2010");

            pobjCo.CustID = lblCustID.Text.Trim();
            BusinessContinousOrder.SearchContInv(pobjCo);
            ViewState.Add("Reshipdt", pobjCo.Dt);
            bindgridUsingViewState();

        }
        catch { }
    }
    private void bindgridUsingViewState()
    {
        try
        {
            GridReShipList.DataSource = ViewState["Reshipdt"];
            GridReShipList.DataBind();

        }
        catch { }
    }
    #endregion


    private void bindOrderProductGrid()
    {
        try
        {
            PropertyCustomerDetail pobj = new PropertyCustomerDetail();
            if (lblCustID.Text.Trim() != "")
            {
                pobj.CustID = lblCustID.Text.Trim();
            }
            BusinessCustomerDetail.selectProductSbyCustId(pobj);
            ViewState["GridProductLog"] = pobj.Dt;
            GridProductLog.DataSource = pobj.Dt;
            GridProductLog.DataBind();

        }
        catch { }
    }
    protected void GridProductLog_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortingDirection = string.Empty;
            if (dir == SortDirection.Ascending)
            {
                dir = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                dir = SortDirection.Ascending;
                sortingDirection = "Asc";
            }
            DataView sortedView = new DataView(sortbindgrid3());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            GridProductLog.DataSource = sortedView;
            GridProductLog.DataBind();

        }
        catch { }
    }
    protected DataTable sortbindgrid3()
    {

        DataTable dt = ViewState["GridProductLog"] as DataTable;
        return dt;
    }
    protected void GridProductLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
          
            for (int rowIndex = GridProductLog.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = GridProductLog.Rows[rowIndex];
                GridViewRow previousRow = GridProductLog.Rows[rowIndex + 1];
                if (((Label)row.FindControl("lnkProInvoiceNo")).Text == ((Label)previousRow.FindControl("lnkProInvoiceNo")).Text && ((Label)row.FindControl("lnkProSkuID")).Text == ((Label)previousRow.FindControl("lnkProSkuID")).Text)
                {
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        string headerRowText = GridProductLog.HeaderRow.Cells[i].Text.Trim();
                        if (!(headerRowText == "Product" || headerRowText == "Product Quantity" || headerRowText == "Product Price"))
                        {
                            if (row.Cells[i].Text == previousRow.Cells[i].Text)
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                                       previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                                if (previousRow.Cells[i].RowSpan >= 2)
                                {
                                }
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }
    }

}

